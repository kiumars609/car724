import React from "react";
import "./scss/carouselProduct.css";
import { Carousel } from "@trendyol-js/react-carousel";

export default function CarouselProduct({ title = "Test Carousel" }) {
  return (
    <>
      <div className="col-md-12 new-trailer mt-5">
        <h3 className="original-title mb-3">New Trailers</h3>
        <Carousel show={3} slide={1} swiping={true}>
          <div className="highl"></div>
          <div className="highl" style={{ backgroundColor: "pink" }}></div>
          <div className="highl" style={{ backgroundColor: "tomato" }}></div>
          <div className="highl" style={{ backgroundColor: "green" }}></div>
        </Carousel>
      </div>
    </>
  );
}


// npm i react react-dom @trendyol-js/react-carousel --save
// https://reactjsexample.com/a-lightweight-carousel-component-for-react/