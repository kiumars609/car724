import React, { useEffect, useRef } from "react";
import Footer from "./Footer/Footer";
import NavBar from "./NavBar/NavBar";
import "./scss/layout.css";

export default function Layout({
  children,
  pageTitle,
  executeAboutUsScroll,
  executeContactUsScroll,
}) {
  useEffect(() => {
    document.title = "AutoRoad724 | " + pageTitle;
  }, [pageTitle]);

  return (
    <>
      <NavBar
        executeAboutUsScroll={executeAboutUsScroll}
        executeContactUsScroll={executeContactUsScroll}
      />
      {children}
      <Footer
        executeAboutUsScroll={executeAboutUsScroll}
        executeContactUsScroll={executeContactUsScroll}
      />

      {/* <!-- loader --> */}
      <div id="ftco-loader" className="show fullscreen d-none">
        <svg className="circular" width="48px" height="48px">
          <circle
            className="path-bg"
            cx="24"
            cy="24"
            r="22"
            fill="none"
            strokeWidth="4"
            stroke="#eeeeee"
          />
          <circle
            className="path"
            cx="24"
            cy="24"
            r="22"
            fill="none"
            strokeWidth="4"
            strokeMiterlimit="10"
            stroke="#F96D00"
          />
        </svg>
      </div>
    </>
  );
}
