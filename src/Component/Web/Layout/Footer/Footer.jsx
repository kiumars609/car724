import React, { version } from "react";
import { Link } from "react-router-dom/dist";

export default function Footer({
  executeAboutUsScroll,
  executeContactUsScroll,
}) {
  return (
    <>
      <footer className="ftco-footer ftco-bg-dark ftco-section">
        <div className="container">
          <div className="row mb-5">
            <div className="col-md">
              <div className="ftco-footer-widget mb-4">
                <h2 className="ftco-heading-2">About Autoroad724</h2>
                <p>
                  Far far away, behind the word mountains, far from the
                  countries Vokalia and Consonantia, there live the blind texts.
                </p>
                <ul className="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                  <li className="ftco-animate">
                    <a href="#">
                      <span className="icon-twitter"></span>
                    </a>
                  </li>
                  <li className="ftco-animate">
                    <a href="#">
                      <span className="icon-facebook"></span>
                    </a>
                  </li>
                  <li className="ftco-animate">
                    <a href="#">
                      <span className="icon-instagram"></span>
                    </a>
                  </li>
                  <li className="ftco-animate">
                    <a href="#">
                      <span className="icon-linkedin"></span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md">
              <div className="ftco-footer-widget mb-4 ml-md-5">
                <h2 className="ftco-heading-2">Information</h2>
                <ul className="list-unstyled">
                  <li>
                    <a
                      onClick={executeAboutUsScroll}
                      role="button"
                      className="py-2 d-block"
                    >
                      About Us
                    </a>
                  </li>
                  <li>
                    <a href="#" className="py-2 d-block">
                      Term and Conditions
                    </a>
                  </li>
                  <li>
                    <a href="#" className="py-2 d-block">
                      Privacy &amp; Cookies Policy
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md">
              <div className="ftco-footer-widget mb-4">
                <h2 className="ftco-heading-2">Customer Support</h2>
                <ul className="list-unstyled">
                  <li>
                    <a href="#" className="py-2 d-block">
                      How it works
                    </a>
                  </li>
                  <li>
                    <a
                      onClick={executeContactUsScroll}
                      role="button"
                      className="py-2 d-block"
                    >
                      Contact Us
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md">
              <div className="ftco-footer-widget mb-4">
                <h2 className="ftco-heading-2">Have a Questions?</h2>
                <div className="block-23 mb-3">
                  <ul>
                    <li>
                      <span className="icon icon-map-marker"></span>
                      <span className="text">
                        23 Sayad Shirazi St. Vakil Abad Blv., Mashhad, Khorasan
                        Razavi, Iran
                      </span>
                    </li>
                    <li>
                      <Link to={`tel:+989155042678`}>
                        <span className="icon icon-phone"></span>
                        <span className="text">+98 915 5042 678</span>
                      </Link>
                    </li>
                    <li>
                      <Link to={`mailto:info@autoroad724.com`}>
                        <span className="icon icon-envelope"></span>
                        <span className="text">info@autoroad724.com</span>
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 text-center">
              <p></p>
              {/* <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --> */}
              Copyright &copy; 2023
              <script>document.write(new Date().getFullYear());</script> All
              rights reserved | This template is developed with{" "}
              <i
                className="icon-heart"
                style={{ color: "red" }}
                aria-hidden="true"
              ></i>{" "}
              by{" "}
              <a href="https://mr-kiumars.ir" target="_blank">
                Kiumars609
              </a>
              {/* <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p> */}
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}
