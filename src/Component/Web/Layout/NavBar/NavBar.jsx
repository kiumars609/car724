import React from "react";
import "./scss/NavBar.css";
import { Link } from "react-router-dom";

export default function NavBar({
  executeAboutUsScroll,
  executeContactUsScroll,
}) {
  return (
    <>
      <nav
        className="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light"
        id="ftco-navbar"
      >
        <div className="container">
          <a className="navbar-brand" href="index.html">
            Auto
            <span>
              road<span className="font-s-2">724</span>
            </span>
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#ftco-nav"
            aria-controls="ftco-nav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="oi oi-menu"></span> Menu
          </button>

          <div className="collapse navbar-collapse" id="ftco-nav">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item active">
                <Link to={"/"} className="nav-link">
                  Home
                </Link>
              </li>
              <li className="nav-item">
                <a
                  onClick={executeAboutUsScroll}
                  className="nav-link"
                  role="button"
                >
                  About Us
                </a>
              </li>
              <li className="nav-item">
                <Link to={"/"} className="nav-link">
                  Blog
                </Link>
              </li>
              <li className="nav-item">
                <a
                  onClick={executeContactUsScroll}
                  className="nav-link"
                  role="button"
                >
                  Contact Us
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}
