import React, { useState, useEffect } from "react";
import { get } from "../../../services";
import { Link } from "react-router-dom";
import { useCommaSeparators } from "../../../CustomHooks/useCommaSeparators";
import { logoes } from "../../Website/HomePage/Products/logoes";
import { useSplitDataToSingleArray } from "../../../CustomHooks/useSplitDataToSingleArray";

export function TypeBackend(param) {
  // Start Image Src
  let carTypeImage = "";
  if (param === "coupe") {
    carTypeImage = "typeCoupe.jpg";
  }

  if (param === "suv") {
    carTypeImage = "typeSUV.jpg";
  }

  if (param === "limousine") {
    carTypeImage = "typeLimousine.jpg";
  }

  if (param === "small-car") {
    carTypeImage = "typeSmallCar.jpg";
  }

  if (param === "pickup") {
    carTypeImage = "typePickup.jpg";
  }

  if (param === "transporter") {
    carTypeImage = "typeTransporter.jpg";
  }

  if (param === "more") {
    carTypeImage = "typeMore.jpg";
  }
  // End Image Src

  // Start Popular Cars
  let carTypePopular = [];
  if (param === "suv") {
    carTypePopular = [
      { name: "VW Tiguan", img: "vw-tiguan.jpg" },
      { name: "Ford Kuga", img: "ford-kuga.jpg" },
      { name: "Hyundai Tucson", img: "hyundai-tucson.jpg" },
      { name: "BMW X1", img: "bmw-x1.jpg" },
      { name: "Nissan Qashqai", img: "nissan-quashqai.jpg" },
      { name: "Audi Q5", img: "audi-q5.jpg" },
      { name: "BMW X3", img: "bmw-x3.jpg" },
      { name: "Audi Q3", img: "audi-q3.jpg" },
      { name: "Opel Mocha", img: "opel-mokka.jpg" },
      { name: "BMW X5", img: "bmw-x5.jpg" },
    ];
  }

  const popularQuery =
    carTypePopular &&
    carTypePopular.map((item, index) => {
      return (
        <div key={index} className="col-md-3 card">
          <div className="col-md-12 inner px-0">
            <img
              src={`/assets/images/typeCars/popular/${item.img}`}
              className="col-md-12"
              alt={item.name}
            />
            <div className="name col-12">{item.name}</div>
          </div>
        </div>
      );
    });
  // End Popular Cars

  // Start Car Offers
  const [data, setData] = useState([]);
  const [loader, setLoader] = useState(true);

  useEffect(() => {
    get("/cars/getCarsAPI.php").then((response) => {
      setData(response);
      setLoader(false);
    });
  }, []);

  const dataQuery =
    data &&
    data.map((item) => {
      const splited = useSplitDataToSingleArray(item.bodyShape);
      if (splited.query2.includes(param)) {
        return (
          <div className="col-md-3" key={item.id}>
            <Link to={`/car-detail/${item.id}`}>
              <div className="car-wrap ">
                <div
                  className="img d-flex align-items-end"
                  style={{
                    backgroundImage: `url(/assets/images/cars/${item.brand}/${item.imageMain})`,
                  }}
                >
                  <div className="price-wrap d-flex">
                    <span className="rate">
                      €{useCommaSeparators(item.price)}
                    </span>
                  </div>
                </div>
                <div className="text p-4 text-left bg-white">
                  <span className="mb-0">{logoes(item.brand)}</span>
                  <h2 className="mb-0">
                    <a href="#">{item.shortTitle}</a>
                  </h2>
                  <span className="mb-3">
                    {useCommaSeparators(item.mileage)} km,{" "}
                    {item.firstRegistration}
                  </span>
                  <span className="mb-0">
                    <i className="fas fa-location"></i>
                    DE 59067 Manheim
                  </span>
                </div>
              </div>
            </Link>
          </div>
        );
      }
    });
  // End Car Offers

  return { carTypeImage, popularQuery, dataQuery, loader };
}
