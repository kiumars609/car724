import React from "react";
import Layout from "../../../Component/Web/Layout/Layout";
import "./scss/style.css";
import { Link, useParams } from "react-router-dom";
import { TypeBackend } from "./TypeBackend";
import Loading from "../../../Component/Loading/Loading";

export default function BodyTypeDetails() {
  const param = useParams();

  const { carTypeImage, popularQuery, dataQuery, loader } = TypeBackend(
    param.type
  );

  return (
    <>
      <Layout pageTitle={`Body Type | ${param.type}`}>
        {/* Start Header Section */}
        <section
          className="hero-wrap hero-wrap-2 js-fullheight"
          style={{
            backgroundImage: `url(/assets/images/background/${carTypeImage})`,
          }}
          data-stellar-background-ratio="0.5"
        >
          <div className="overlay"></div>
          <div className="container">
            <div className="row no-gutters slider-text js-fullheight align-items-end justify-content-start">
              <div className="col-md-9 pb-5">
                <p className="breadcrumbs">
                  <span className="mr-2">
                    <Link to={"/"}>
                      Home <i className="ion-ios-arrow-forward"></i>
                    </Link>
                  </span>
                  <span>
                    Cars by body type <i className="ion-ios-arrow-forward"></i>
                  </span>
                </p>
                <h1 className="mb-3 bread">{param.type}</h1>
              </div>
            </div>
          </div>
        </section>
        {/* End Header Section */}

        {/* Start Most Popular Cars */}
        <section className="col-md-10 mx-auto mb-2 main-most-popular bg-white">
          <h1 className="title">
            The most popular <span>{param.type}</span>s at a glance
          </h1>
          <div className="col-md-10 main-cards mx-auto px-0">
            {popularQuery}
          </div>
        </section>
        {/* End Most Popular Cars */}

        {/* Start Current Offers */}
        <section className="col-md-10 mx-auto mb-2 main-most-popular bg-white">
          <h1 className="title">
            Current offers for <span>{param.type}</span>s
          </h1>
          <section className="ftco-section pt-1">
            <div className="container-fluid px-4">
              <div className="row">
                {loader ? (
                  <div
                    className="col-md-12"
                    style={{ height: "600px", background: "#000" }}
                  >
                    <Loading />
                  </div>
                ) : (
                  dataQuery
                )}
              </div>
            </div>
          </section>
        </section>
        {/* End Current Offers */}

        {/* Start Explain */}
        <section className="col-md-10 mx-auto mb-2 main-most-popular bg-white">
          <h1 className="title">
            What does <span>{param.type}</span> mean?
          </h1>
          <p>
            Whether mini, compact or large - SUVs are the modern lifestyle
            alternative to vans and are not only favorites with families . The
            high seating position and ease of entry and exit are what make SUVs
            stand out for many buyers. And SUVs are also extremely helpful for
            use in the city when children, shopping or luggage need to be
            transported, but only a small parking space is available. What does
            SUV mean? The term SUV has existed since the 1990s and emerged with
            the first models such as the Toyota RAV4 or the Land Rover
            Freelander. The first forerunners of the so-called Sport Utility
            Vehicles were already in the 1960s and 1970s with the Jeep Wagoneer
            from 1963 and the Range Rover from 1970. Compared to these models,
            however, today's SUVs have significant differences: An SUV usually
            only looks good with its off-road character and otherwise has more
            of the comfort of a sedan. In most cases, SUVs are technically only
            suitable for off-road use to a limited extent . That was different
            with the SUV precursors because they were designed for gravel,
            stones and inclines like off-road vehicles. Today, self-supporting
            bodies are generally used in SUVs and not ladder frames as in
            off-road vehicles, which considerably restricts off-road mobility.
            Larger SUVs in particular are therefore more likely to be considered
            prestige cars with visual advantages . Nevertheless, some SUV models
            also have strong driving performance due to their size, weight and
            all-wheel drive and are often used as transport or towing vehicles
            for trailers. Compared to sedans, SUVs also have a large luggage
            compartment volume and the space available is generally
            significantly larger . Similar to vans, some SUV models are even
            offered as seven or nine-seaters , making them particularly popular
            with larger families. The elevated seating position also allows
            better all-round visibility in traffic . However, because there are
            also very small SUV models for use on short journeys or in the city,
            the range of uses among SUVs is very large : subcategories such as
            compact or mini SUVs underline the strong trend in the
            cross-lifestyle segment in recent years . What SUVs are there?
            Depending on the application, there are suitable SUVs on the market.
            Models then differ in terms of drive, equipment or structure .
            Before deciding on an SUV, buyers should therefore ask themselves:
            Should the vehicle be off-road? Will the SUV only be driven on paved
            roads? Do I occasionally drive the SUV into the mountains? Do I want
            to get from A to B as economically as possible? SUV drive If you
            only drive on paved roads, you basically don't need more than a
            normal front-wheel drive . Anyone who occasionally drives into the
            mountains could benefit from a switchable or permanent all-wheel
            drivebenefit. Compact SUVs such as the Honda CR-V or the Toyota RAV
            4 generally drive with front-wheel drive, but can switch to
            all-wheel drive if the surface requires it. With all-wheel drive -
            whether switched on or permanent - you have to accept higher
            consumption: Because SUVs are usually more than 100 kg heavier and
            there are additional friction losses in the drive train. Therefore,
            an SUV with all-wheel drive makes little sense in city or road
            traffic and should be more of a focus if you travel a lot off-road
            or want to transport trailers with the SUV. Hybrid SUV or electric
            SUV? Most SUV models are now not only available as combustion
            engines, but also as hybrids or purely electric SUVs . Hybrid and
            electric drives in SUVs help to save fuel and reduce CO2 emissions.
            Hybrid SUVs with an electric motor can draw on more system
            performance, especially in the middle and luxury classes. As a mild
            hybrid, full hybrid or plug-in hybrid, they also use the horsepower
            of the electric motor for faster acceleration or for a booster
            function, for example when overtaking. While pure electric vehicles
            are dependent on charging stops, the combustion engine takes over
            the drive in the hybrid with an empty power storage unit. In terms
            of overall range, a hybrid car can therefore be compared with a
            conventional petrol or diesel engine. SUV dimensions In addition,
            the design and size of the SUV is crucial. For Europe, the full-size
            SUVs that have been selling consistently well off the assembly line
            in the USA for decades have proven to be too big and fuel-intensive.
            Many car manufacturers are therefore developing smaller SUV series
            that adopt the visual elements of an SUV, but are much more compact
            in their dimensions. But the transition between a large and a
            compact SUV is often fluid, as can be seen in the BMW X5 and X3
            models, which differ in length by only 20 centimeters - but the X5
            is one of the "regular" SUVs and the X3 to the compact SUV. For
            comparison: in the Audi Q7 and Q5 models, the difference in length
            is twice as great. Large SUV or compact SUV? The smaller compact
            SUVs have been booming since the late 1990s and soon became the
            focus of many car manufacturers. Just as with the larger SUV
            brothers, the feeling of space and the raised seating position are
            among the popular properties of compact SUVs, but with some
            significant advantages in terms of weight and maintenance costs .
            Asian manufacturers of compact SUVs are Honda with the HR-V or
            Hyundai with the Tucson and its successor Hyundai ix35 . Competing
            German manufacturers Audi and BMW are represented in the compact SUV
            segment with models such as the Audi Q3 and the BMW X3 and X1 .
            Volkswagen manufactured the VW Tiguan as the entry-level model in
            the SUV class, while the Czech subsidiary included the Skoda Yeti in
            its range. Compact SUVs such as the Renault Koleos and the Nissan
            Qashqai came onto the market from Renault-Nissan. The Renault
            subsidiary Dacia brought models like the Duster and Peugeot the
            models3008 and 4007 / 4008 out. mini SUV Since the vehicle classes
            are also being broken down further and further in the case of SUVs,
            mini SUVs can sometimes hardly be distinguished from compact SUVs.
            Because here, too, the boundaries can be fluid. The term mini-SUV
            gives a name to the growing group of four-meter SUVs . Like its big
            brothers, a mini SUV has more ground clearance thanks to a raised
            chassis and larger wheels. As a result, occupants in mini SUVs also
            sit in an elevated position. There are properties from completely
            different worlds that mini SUVs offer: the style and seating
            position of an SUV, the space and flexibility of a van and the
            agility of a compact sedan.
          </p>
        </section>
        {/* End Explain */}
      </Layout>
    </>
  );
}
