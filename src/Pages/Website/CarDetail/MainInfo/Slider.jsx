import React from "react";

export default function Slider({item}) {
  return (
    <>
      <div className="col-md-8">
        <div className="car-details">
          {/* <div
                    className="img"
                    style={{ backgroundImage: `URL(/assets/images/bg_1.jpg)` }}
                  ></div> */}
          {/* Start Slider */}
          <div
            id="carouselExampleDark"
            className="carousel carousel-dark slide"
            data-bs-ride="carousel"
          >
            <div className="carousel-indicators">
              <img src={`/assets/images/cars/${item.brand}/${item.imageMain}`}
              type="button"
              data-bs-target="#carouselExampleDark"
              data-bs-slide-to="0"
              className="active"
              aria-current="true"
              aria-label="Slide 1"
              />
              <img src={`/assets/images/cars/${item.brand}/${item.imageSecond}`}
              type="button"
              data-bs-target="#carouselExampleDark"
              data-bs-slide-to="1"
              aria-label="Slide 2"
              />
              <img src={`/assets/images/cars/${item.brand}/${item.imageThird}`}
              type="button"
              data-bs-target="#carouselExampleDark"
              data-bs-slide-to="2"
              aria-label="Slide 3"
              />
              <img src={`/assets/images/cars/${item.brand}/${item.imageFourth}`}
              type="button"
              data-bs-target="#carouselExampleDark"
              data-bs-slide-to="3"
              aria-label="Slide 4"
              />
            </div>
            <div className="carousel-inner">
              <div className="carousel-item active" data-bs-interval="10000">
                <img
                  src={`/assets/images/cars/${item.brand}/${item.imageMain}`}
                  className="d-block w-100"
                  alt="..."
                />
              </div>
              <div className="carousel-item">
                <img
                  src={`/assets/images/cars/${item.brand}/${item.imageSecond}`}
                  className="d-block w-100"
                  alt="..."
                />
              </div>
              <div className="carousel-item">
                <img
                  src={`/assets/images/cars/${item.brand}/${item.imageThird}`}
                  className="d-block w-100"
                  alt="..."
                />
              </div>
              <div className="carousel-item">
                <img
                  src={`/assets/images/cars/${item.brand}/${item.imageFourth}`}
                  className="d-block w-100"
                  alt="..."
                />
              </div>
            </div>
            <button
              className="carousel-control-prev"
              type="button"
              data-bs-target="#carouselExampleDark"
              data-bs-slide="prev"
            >
              <span
                className="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Previous</span>
            </button>
            <button
              className="carousel-control-next"
              type="button"
              data-bs-target="#carouselExampleDark"
              data-bs-slide="next"
            >
              <span
                className="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Next</span>
            </button>
          </div>
          {/* End Slider */}
        </div>
      </div>
    </>
  );
}
