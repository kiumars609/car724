import React from "react";
import { useCommaSeparators } from "../../../../CustomHooks/useCommaSeparators";
import { Link } from "react-router-dom";

export default function Info({ item }) {
  return (
    <>
      <div className="col-md-4 main-info px-0">
        <div className="col-12 d-flex px-0 options">
          <div className="col-md-4">
            <span>
              <i className="fa fa-star-o"></i> Notic
            </span>
          </div>
          <div className="col-md-4">
            <span>
              <i className="fa fa-share-alt"></i> Split
            </span>
          </div>
          <div className="col-md-4">
            <span>
              <i className="fa fa-print"></i> Press
            </span>
          </div>
        </div>
        <div className="container">
          <h3 className="fw-bold mt-3">{item.shortTitle}</h3>
          <h5>{item.explainTitle}</h5>
          <p className="location-icon">
            <i className="fas fa-map-marker-alt"></i> DE 59067 Manheim
          </p>
          <hr />
          {/* Price Section */}
          <div className="d-flex justify-content-between price-section">
            <span className="price">
              € {useCommaSeparators(String(item.price))}
            </span>
            <div className="col-5 price-label-bar">
              <span>Very Good Price</span>
              <div className="bar col-md-12"></div>
              <img
                className="col-md-12 mt-5 procheck-icon"
                src="/assets/images/icon/procheck.png"
                alt="procheck24"
              />
            </div>
          </div>
          {/* End Price Section */}
          <hr />
          {/* Insurance Section */}
          <div className="d-flex justify-content-between insurance-section">
            <div className="div col-md-7 pl-0">
              <span className="col-md-12 pl-0 text-1">
                <span>from €14.02</span> monthly
              </span>
              <br />
              <span className="price col-md-12 pl-0 text-2">
                Car insurance details <Link to={"#"}>here</Link>
              </span>
            </div>
            <div className="col-5 insurance-label-bar">
              <img
                className="col-md-10 mt-3 procheck-icon"
                src="/assets/images/icon/check.png"
                alt="check24"
              />
            </div>
          </div>
          {/* End Insurance Section */}
          <hr />
          <div className="col-md-12 d-flex justify-content-between px-0">
            <Link to={`mailto:qumarsgamer@gmail.com`} className="btn btn-infos">
              Send e-mail
            </Link>
            <Link to={`tel:+9155042678`} className="btn btn-infos">
              +49 (0)2163 - 9794002
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}
