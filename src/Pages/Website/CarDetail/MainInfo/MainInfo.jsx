import React from "react";
import Slider from "./Slider";
import Info from "./Info";

export default function MainInfo({ item }) {
  return (
    <>
      <div className="row justify-content-center">
        <Slider item={item} />
        <Info item={item} />
      </div>
    </>
  );
}
