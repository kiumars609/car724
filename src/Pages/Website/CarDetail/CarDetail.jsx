import React, { useState, useEffect } from "react";
import Layout from "../../../Component/Web/Layout/Layout";
import { Link, useParams } from "react-router-dom";
import { get } from "../../../services";
import "./scss/singleProduct.css";
import MainInfo from "./MainInfo/MainInfo";
import ShortInfo from "./ShortInfo";
import AllInfo from "./AllInfo/";
import RelatedCars from "./RelatedCars";
import Loading from "../../../Component/Loading";
import { useCommaSeparators } from "../../../CustomHooks/useCommaSeparators";

export default function CarDetail() {
  const param = useParams();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    get("/cars/getCarsAPI.php")
      .then((response) => {
        for (let index = 0; index < response.length; index++) {
          if (response[index].id === param.id) {
            const element = response[index];
            setData(element);
            setLoading(false);
          }
        }
      })
      .catch(function(error) {
        console.log(error);
      })
      .then(function() {
        // always executed
      });
  }, [param]);

  const item = data;
  return (
    <>
      {/* Nav Scrolled Short Detail Car */}
      {loading ? (
        ""
      ) : (
        <nav
          className="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light"
          id="ftco-navbar"
        >
          <div className="col-md-12 mx-auto nav-scrolled-detail-auto">
            <div className="row px-0">
              <div className="col-md-2 px-0">
                <div className="col-md-12 mh-10 px-0 d-flex align-items-center main-image">
                  <img
                    src={`/assets/images/cars/${item.brand}/${item.imageMain}`}
                    className="col-md-12"
                  />
                </div>
              </div>
              <div className="col-md-4 main-titles">
                <h3 className="mt-3">{item.shortTitle}</h3>
                <h5>{item.explainTitle}</h5>
              </div>
              <div className="col-md-2 price-section d-flex align-items-center px-0">
                <span className="price">
                  €{useCommaSeparators(String(item.price))}
                </span>
              </div>
              <div className="col-md-2 options d-flex align-items-center pl-1 pr-0">
                <div class="col-md-6 px-0">
                  <span>
                    <i class="fa fa-star-o"></i> Notic
                  </span>
                </div>
                <div class="col-md-6 px-0">
                  <span>
                    <i class="fa fa-share-alt"></i> Split
                  </span>
                </div>
              </div>
              <div className="col-md-2 options d-flex align-items-center pl-1 pr-0">
                <Link class="btn btn-infos" href="tel:+9155042678">
                  Contact the Provider
                </Link>
              </div>
            </div>
          </div>
        </nav>
      )}
      {/* End Nav Scrolled Short Detail Car */}

      <Layout pageTitle={item.shortTitle}>
        {loading ? (
          <div
            className="col-md-12"
            style={{ height: "600px", background: "#000" }}
          >
            <Loading />
          </div>
        ) : (
          <>
            <section className="ftco-section ftco-car-details">
              <div className="container p-3">
                <MainInfo item={item} />
                <ShortInfo item={item} />
                <AllInfo item={item} />
              </div>
            </section>
            <RelatedCars item={item} param={param} />
          </>
        )}
      </Layout>
    </>
  );
}
