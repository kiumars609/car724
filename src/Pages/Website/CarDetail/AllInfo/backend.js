import React from "react";
import { useCommaSeparators } from "../../../../CustomHooks/useCommaSeparators";
import { logoes } from "../../HomePage/Products/logoes";

export function backend(item) {
  const basicData = [
    {
      name: "Brand",
      data: [item.brand.toUpperCase() + " ", logoes(item.brand, "30px")],
    },
    { name: "Body shape", data: item.bodyShape },
    { name: "Drive type", data: item.driveType },
    { name: "Seats", data: item.seats },
    { name: "Doors", data: item.doors },
    { name: "Country version", data: item.countryVersion },
    { name: "Guarantee", data: item.guarantee },
  ];

  const vehicleHistory = [
    { name: "Mileage", data: useCommaSeparators(item.mileage)+'km' },
    { name: "First registration", data: item.firstRegistration },
    { name: "Vehicle owner", data: item.vehicleOwner },
  ];

  const technicalSpecifications = [
    { name: "Performance", data: item.performance },
    { name: "Transmission", data: item.transmission },
    {
      name: "Displacement",
      data: useCommaSeparators(String(item.displacement)) + "cc",
    },
  ];

  const powerConsumption = [
    { name: "Fuel", data: item.fuel },
    { name: "Energy efficiency class", data: item.energyEfficiencyClass },
    { name: "Emission class", data: item.emissionClass },
    { name: "CO₂ efficiency", co2: true, co2Data: item.energyEfficiencyClass },
  ];

  const furnishing = [
    { name: "Comfort", data: item.comfort },
    { name: "Entertainment/Media", data: item.media },
    { name: "Security", data: item.security },
    { name: "Extras", data: item.extras },
  ];

  const colorInteriorDesign = [
    { name: "Exterior color", data: item.exteriorColor },
    { name: "Livery", data: item.livery },
    { name: "Interior color", data: item.interiorColor },
    { name: "Interior design", data: item.interiorDesign },
    { name: "Special equipment", data: item.specialEquipment },
  ];

  return {
    basicData,
    vehicleHistory,
    technicalSpecifications,
    powerConsumption,
    furnishing,
    colorInteriorDesign,
  };
}
