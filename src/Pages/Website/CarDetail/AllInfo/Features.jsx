import React from "react";
import Co2Table from "./Co2Table";

export default function Features({ font, title, data }) {
  const headerQuery =
    data &&
    data.map((item, index) => {
      return (
        <div key={index} className="col-md-12 headers">
          {item.name}
        </div>
      );
    });

  const resultQuery =
    data &&
    data.map((item, index) => {
      return (
        <>
          {item.co2 ? (
            <Co2Table item={item.co2Data} />
          ) : (
            <div key={index} className="col-md-12 result">
              {item.data}
            </div>
          )}
        </>
      );
    });

  return (
    <>
      <div className="col-md-12 main-features d-flex py-4">
        <div className="col-md-4">
          <h5 className="title">
            <i className={font}></i> {title}
          </h5>
        </div>
        <div className="col-md-3">{headerQuery}</div>
        <div className="col-md-5 pl-0">{resultQuery}</div>
      </div>
    </>
  );
}
