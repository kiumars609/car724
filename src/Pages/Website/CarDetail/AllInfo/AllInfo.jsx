import React from "react";
import Features from "./Features";
import { backend } from "./backend";

export default function AllInfo({ item }) {
  const backData = backend(item);
  return (
    <>
      <div className="row">
        <div className="col-md-12 pills">
          <div className="bd-example bd-example-tabs">
            <div className="d-flex justify-content-center">
              <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li className="nav-item">
                  <a
                    className="nav-link active"
                    id="pills-description-tab"
                    data-toggle="pill"
                    href="#pills-description"
                    role="tab"
                    aria-controls="pills-description"
                    aria-expanded="true"
                  >
                    Features
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className="nav-link"
                    id="pills-manufacturer-tab"
                    data-toggle="pill"
                    href="#pills-manufacturer"
                    role="tab"
                    aria-controls="pills-manufacturer"
                    aria-expanded="true"
                  >
                    Description
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className="nav-link"
                    id="pills-review-tab"
                    data-toggle="pill"
                    href="#pills-review"
                    role="tab"
                    aria-controls="pills-review"
                    aria-expanded="true"
                  >
                    Review
                  </a>
                </li>
              </ul>
            </div>

            <div className="tab-content" id="pills-tabContent">
              <div
                className="tab-pane fade show active px-0"
                id="pills-description"
                role="tabpanel"
                aria-labelledby="pills-description-tab"
              >
                {/* Basic Data */}
                <Features
                  font={"fa fa-car-side"}
                  title={"Basic Data"}
                  data={backData.basicData}
                />
                {/* Vehicle History */}
                <Features
                  font={"fa fa-history"}
                  title={"Vehicle History"}
                  data={backData.vehicleHistory}
                />
                {/* Technical Specifications */}
                <Features
                  font={"fa fa-wrench"}
                  title={"Technical Specifications"}
                  data={backData.technicalSpecifications}
                />
                {/* Power Consumption */}
                <Features
                  font={"fa fa-gas-pump"}
                  title={"Power Consumption"}
                  data={backData.powerConsumption}
                />
                {/* Furnishing */}
                <Features
                  font={"fa fa-chair"}
                  title={"Furnishing"}
                  data={backData.furnishing}
                />
                {/* color and interior design */}
                <Features
                  font={"fas fa-palette"}
                  title={"Color and Interior Design"}
                  data={backData.colorInteriorDesign}
                />
              </div>

              <div
                className="tab-pane fade"
                id="pills-manufacturer"
                role="tabpanel"
                aria-labelledby="pills-manufacturer-tab"
              >
                <p>{item.description}</p>
              </div>

              <div
                className="tab-pane fade"
                id="pills-review"
                role="tabpanel"
                aria-labelledby="pills-review-tab"
              >
                <div className="row">
                  <div className="col-md-7">
                    <h3 className="head">23 Reviews</h3>
                    <div className="review d-flex">
                      <div
                        className="user-img"
                        style={{
                          backgroundImage: `URL(images/person_1.jpg)`,
                        }}
                      ></div>
                      <div className="desc">
                        <h4>
                          <span className="text-left">Jacob Webb</span>
                          <span className="text-right">14 March 2018</span>
                        </h4>
                        <p className="star">
                          <span>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                          </span>
                          <span className="text-right">
                            <a href="#" className="reply">
                              <i className="icon-reply"></i>
                            </a>
                          </span>
                        </p>
                        <p>
                          When she reached the first hills of the Italic
                          Mountains, she had a last view back on the skyline of
                          her hometown Bookmarksgrov
                        </p>
                      </div>
                    </div>
                    <div className="review d-flex">
                      <div
                        className="user-img"
                        style={{
                          backgroundImage: `URL(images/person_2.jpg)`,
                        }}
                      ></div>
                      <div className="desc">
                        <h4>
                          <span className="text-left">Jacob Webb</span>
                          <span className="text-right">14 March 2018</span>
                        </h4>
                        <p className="star">
                          <span>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                          </span>
                          <span className="text-right">
                            <a href="#" className="reply">
                              <i className="icon-reply"></i>
                            </a>
                          </span>
                        </p>
                        <p>
                          When she reached the first hills of the Italic
                          Mountains, she had a last view back on the skyline of
                          her hometown Bookmarksgrov
                        </p>
                      </div>
                    </div>
                    <div className="review d-flex">
                      <div
                        className="user-img"
                        style={{
                          backgroundImage: `URL(images/person_3.jpg)`,
                        }}
                      ></div>
                      <div className="desc">
                        <h4>
                          <span className="text-left">Jacob Webb</span>
                          <span className="text-right">14 March 2018</span>
                        </h4>
                        <p className="star">
                          <span>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                          </span>
                          <span className="text-right">
                            <a href="#" className="reply">
                              <i className="icon-reply"></i>
                            </a>
                          </span>
                        </p>
                        <p>
                          When she reached the first hills of the Italic
                          Mountains, she had a last view back on the skyline of
                          her hometown Bookmarksgrov
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-5">
                    <div className="rating-wrap">
                      <h3 className="head">Give a Review</h3>
                      <div className="wrap">
                        <p className="star">
                          <span>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            (98%)
                          </span>
                          <span>20 Reviews</span>
                        </p>
                        <p className="star">
                          <span>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            (85%)
                          </span>
                          <span>10 Reviews</span>
                        </p>
                        <p className="star">
                          <span>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            (70%)
                          </span>
                          <span>5 Reviews</span>
                        </p>
                        <p className="star">
                          <span>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            (10%)
                          </span>
                          <span>0 Reviews</span>
                        </p>
                        <p className="star">
                          <span>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            <i className="ion-ios-star"></i>
                            (0%)
                          </span>
                          <span>0 Reviews</span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
