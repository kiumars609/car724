import React from "react";

export default function Co2Table({ item }) {
  return (
    <>
      <div className="col-md-7 main-co2">
        <p>
          Calculated on the basis of the measured CO₂ emissions, taking into
          account the mass of the vehicle.
        </p>
        <span className="aFirst">
          A+++ <div className="arrow"></div>{" "}
          {item && item === "A+++" && (
            <div className="selected">
              {item} <div className="arrow-selected"></div>
            </div>
          )}
        </span>
        <span className="aSecond">
          A++ <div className="arrow"></div>{" "}
          {item && item === "A++" && (
            <div className="selected">
              {item} <div className="arrow-selected"></div>
            </div>
          )}
        </span>
        <span className="aThird">
          A+ <div className="arrow"></div>{" "}
          {item && item === "A+" && (
            <div className="selected">
              {item} <div className="arrow-selected"></div>
            </div>
          )}
        </span>
        <span className="aEnergy">
          A <div className="arrow"></div>{" "}
          {item && item === "A" && (
            <div className="selected">
              {item} <div className="arrow-selected"></div>
            </div>
          )}
        </span>
        <span className="bEnergy">
          B <div className="arrow"></div>{" "}
          {item && item === "B" && (
            <div className="selected">
              {item} <div className="arrow-selected"></div>
            </div>
          )}
        </span>
        <span className="cEnergy">
          C <div className="arrow"></div>{" "}
          {item && item === "C" && (
            <div className="selected">
              {item} <div className="arrow-selected"></div>
            </div>
          )}
        </span>
        <span className="dEnergy">
          D <div className="arrow"></div>{" "}
          {item && item === "D" && (
            <div className="selected">
              {item} <div className="arrow-selected"></div>
            </div>
          )}
        </span>
        <span className="eEnergy">
          E <div className="arrow"></div>{" "}
          {item && item === "E" && (
            <div className="selected">
              {item} <div className="arrow-selected"></div>
            </div>
          )}
        </span>
        <span className="fEnergy">
          F <div className="arrow"></div>{" "}
          {item && item === "F" && (
            <div className="selected">
              {item} <div className="arrow-selected"></div>
            </div>
          )}
        </span>
        <span className="gEnergy">
          G <div className="arrow"></div>{" "}
          {item && item === "G" && (
            <div className="selected">
              {item} <div className="arrow-selected"></div>
            </div>
          )}
        </span>
      </div>
    </>
  );
}
