import React, { useEffect, useState } from "react";
import { get } from "../../../services";
import { useCommaSeparators } from "../../../CustomHooks/useCommaSeparators";
import { Link } from "react-router-dom";
import { logoes } from "../HomePage/Products/logoes";

export default function RelatedCars({ item, param }) {
  const [data, setData] = useState([]);

  useEffect(() => {
    get("/cars/getCarsAPI.php").then((response) => setData(response));
  }, [item]);

  const query =
    data &&
    data
      .filter((filter) => filter.brand === item.brand)
      .slice(0, 4)
      .map((itm) => {
        if (itm.id !== param.id) {
          return (
            <div className="col-md-3" key={itm.id}>
              <Link to={`/car-detail/${itm.id}`}>
                <div className="car-wrap ">
                  <div
                    className="img d-flex align-items-end"
                    style={{
                      backgroundImage: `url(/assets/images/cars/${itm.brand}/${itm.imageMain})`,
                    }}
                  >
                    <div className="price-wrap d-flex">
                      <span className="rate">
                        €{useCommaSeparators(itm.price)}
                      </span>
                    </div>
                  </div>
                  <div className="text p-4 text-left bg-white">
                    <span className="mb-0">{logoes(itm.brand)}</span>
                    <h2 className="mb-0">
                      <a href="#">{itm.shortTitle}</a>
                    </h2>
                    <span className="mb-3">
                      {useCommaSeparators(itm.mileage)} km,{" "}
                      {itm.firstRegistration}
                    </span>
                    <span className="mb-0">
                      <i className="fas fa-location"></i>
                      DE 59067 Manheim
                    </span>
                  </div>
                </div>
              </Link>
            </div>
          );
        }
      });

  return (
    <>
      <section className="ftco-section pt-1">
        <div className="container-fluid px-4">
          <div className="row justify-content-center">
            <div className="col-md-12 heading-section text-center mb-5">
              <span className="subheading">Choose Car</span>
              <h2 className="mb-2">Related Cars</h2>
            </div>
          </div>
          <div className="row">{query}</div>
        </div>
      </section>
    </>
  );
}
