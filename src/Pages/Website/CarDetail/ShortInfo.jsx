import React from "react";
import { useCommaSeparators } from "../../../CustomHooks/useCommaSeparators";

export default function ShortInfo({ item }) {
  return (
    <>
      <div className="row">
        <div className="col-md d-flex align-self-stretch">
          <div className="media block-6 services">
            <div className="media-body py-md-4">
              <div className="d-flex mb-3 align-items-center">
                <div className="icon">
                  <span className="flaticon-dashboard"></span>
                </div>
                <div className="text">
                  <h3 className="heading mb-0 pl-3">
                    Mileage
                    <span>{useCommaSeparators(String(item.mileage))}km</span>
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md d-flex align-self-stretch">
          <div className="media block-6 services">
            <div className="media-body py-md-4">
              <div className="d-flex mb-3 align-items-center">
                <div className="icon">
                  <span className="flaticon-car-machine"></span>
                </div>
                <div className="text">
                  <h3 className="heading mb-0 pl-3">
                    Transmission
                    <span>{item.transmission}</span>
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md d-flex align-self-stretch">
          <div className="media block-6 services">
            <div className="media-body py-md-4">
              <div className="d-flex mb-3 align-items-center">
                <div className="icon">
                  <span className="flaticon-car-seat"></span>
                </div>
                <div className="text">
                  <h3 className="heading mb-0 pl-3">
                    Seats
                    <span>{item.seats} Adults</span>
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md d-flex align-self-stretch">
          <div className="media block-6 services">
            <div className="media-body py-md-4">
              <div className="d-flex mb-3 align-items-center">
                <div className="icon">
                  <span className="flaticon-calendar"></span>
                  <i className="fa fa-calendar fs-3"></i>
                </div>
                <div className="text">
                  <h3 className="heading mb-0 pl-3">
                    First Registration
                    <span>{item.firstRegistration}</span>
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md d-flex align-self-stretch">
          <div className="media block-6 services">
            <div className="media-body py-md-4">
              <div className="d-flex mb-3 align-items-center">
                <div className="icon">
                  <span className="flaticon-diesel"></span>
                </div>
                <div className="text">
                  <h3 className="heading mb-0 pl-3">
                    Fuel
                    <span>{item.fuel}</span>
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
