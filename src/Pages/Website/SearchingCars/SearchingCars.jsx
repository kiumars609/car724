import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Layout from "../../../Component/Web/Layout";
import { get } from "../../../services";
import { useCommaSeparators } from "../../../CustomHooks/useCommaSeparators";
import { logoes } from "../../../Pages/Website/HomePage/Products/logoes";
import "./scss/searchingCars.css";
import Loading from "../../../Component/Loading/Loading";

export default function SearchingCars() {
  const params = useParams();
  const [data, setData] = useState([]);
  const [loader, setLoader] = useState(true);

  useEffect(() => {
    get("/cars/getCarsAPI.php").then((response) => {
      setData(response);
      setLoader(false);
    });
  }, []);

  const query =
    data &&
    data
      .filter((filter) =>
        params.model !== "all"
          ? filter.brand === params.brand && filter.model === params.model
          : filter.brand === params.brand
      )
      .map((item) => {
        return (
          <>
            <div className="col-md-3" key={item.id}>
              <Link to={`/car-detail/${item.id}`}>
                <div className="car-wrap ">
                  <div
                    className="img d-flex align-items-end"
                    style={{
                      backgroundImage: `url(/assets/images/cars/${item.brand}/${item.imageMain})`,
                    }}
                  >
                    <div className="price-wrap d-flex">
                      <span className="rate">
                        €{useCommaSeparators(item.price)}
                      </span>
                    </div>
                  </div>
                  <div className="text p-4 text-left">
                    <span className="mb-0">{logoes(item.brand)}</span>
                    <h2 className="mb-0">
                      <a href="#">{item.shortTitle}</a>
                    </h2>
                    <span className="mb-3">
                      {useCommaSeparators(item.mileage)} km,{" "}
                      {item.firstRegistration}
                    </span>
                    <span className="mb-0">
                      <i className="fas fa-location"></i>
                      {/* <i className="fas fa-map-marker"></i> */}
                      DE 59067 Manheim
                    </span>
                  </div>
                </div>
              </Link>
            </div>
          </>
        );
      });

  return (
    <>
      <Layout pageTitle={`results | ${params.model}`}>
        {/* Start Header Section */}
        <section
          className="hero-wrap hero-wrap-2 js-fullheight searching-car"
          style={{
            backgroundImage: `url(/assets/images/background/${params.brand}-banner.jpg)`,
          }}
          data-stellar-background-ratio="0.5"
        >
          <div className="overlay"></div>
        </section>
        {/* End Header Section */}

        {loader ? (
          <div
            className="col-md-12"
            style={{ height: "600px", background: "#000" }}
          >
            <Loading />
          </div>
        ) : (
          <section className="ftco-section">
            <div className="container-fluid px-4">
              <div className="row justify-content-center">
                <div className="col-md-12 heading-section text-center mb-5">
                  <h2 className="mb-2">
                    Choose Your
                    <span className="text-uppercase">
                      {" "}
                      {params.model !== "all" ? params.model : params.brand}
                    </span>
                  </h2>
                </div>
              </div>
              <div className="row">{query}</div>
            </div>
          </section>
        )}
      </Layout>
    </>
  );
}
