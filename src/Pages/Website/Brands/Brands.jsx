import React from "react";
import "./scss/brands.css";
import { useParams } from "react-router-dom";
import Layout from "../../../Component/Web/Layout";
import { fullInfo } from "./fullInfo";
import FormBox from "../HomePage/Slider/FormBox";

export default function Brands() {
  const { brand } = useParams();
  console.log(brand);
  const { query } = fullInfo(brand);

  // Start Sorts
  const sorts = [
    { name: "small car" },
    { name: "limousine" },
    { name: "convertible" },
    { name: "station wagon" },
    { name: "SUV" },
    { name: "van" },
    { name: "pick up" },
    { name: "coupe" },
  ];

  const sortsQuery =
    sorts &&
    sorts.map((item, index) => {
      return (
        <a href="#" className="option" key={index}>
          <span>{item.name}</span>
        </a>
      );
    });
  // End Sorts

  // Start Models
  const modelsQuery =
    query &&
    query["models"].map((item, index) => {
      return (
        <div className="col-md-3 card px-0" key={index}>
          <img
            src={`/assets/images/cars/${brand}/${item.img}`}
            className="col-md-12 px-0"
            alt={item.name}
          />
          <div className="transition-info">{brand + " " + item.name}</div>
        </div>
      );
    });
  // End Models

  // Start Top Models
  const topModelsQuery =
    query &&
    query["topModels"].map((item, index) => {
      return (
        <div className="col-md-3 link" key={index}>
          <a href="#">
            <i className="fa fa-angle-right"></i> {item.name}
          </a>
        </div>
      );
    });
  // End Top Models

  return (
    <>
      <Layout pageTitle={brand.toUpperCase()}>
        <div className="container-fluid brands px-0">
          {/* Start Header */}
          <section className="col-md-11 mx-auto brands-header">
            <div className="col-md-12 title-brand">
              <img src={`/assets/images/logo/${query.img}`} alt={brand} />
              <p className="brand-name">
                {brand}
                <p className="slogan">{query.motto}</p>
              </p>
            </div>
          </section>
          {/* Start Header */}
          {/* Start Sorts */}
          <section className="mt-3 col-md-11 mx-auto brands-sort">
            <h3 className="brands-sort-title">{brand}</h3>
            <p className="brands-sort-sub-title">
              Find the model that suits you now.
            </p>
            <div className="col-md-12 brands-sort-options">{sortsQuery}</div>
          </section>
          {/* End Sorts */}
          {/* Start Top Models */}
          <section className=" col-md-11 mx-auto brands-models">
            <div className="col-md-12 brands-models-cards px-0">
              {modelsQuery}
            </div>
          </section>
          {/* End Top Models */}
          {/* Start Short Describe */}
          <section className=" col-md-11 mx-auto mt-3 brands-short-describe">
            <h3 className="brands-sort-title px-0 ml-0">{brand}</h3>
            <span>
              {query.shortDescribe + " "}
              <a href="#tofff">Continue reading</a>
            </span>
          </section>
          {/* End Short Describe */}
          {/* Start Models at a glance */}
          <section className=" col-md-11 mx-auto mt-3 py-3 pl-4 brands-models-glance">
            <h3 className="brands-sort-title px-0 ml-0 fs-5">
              {brand} models at a glance
            </h3>
            <p className="mb-0">Current top models</p>
            <div className="col-md-12 d-flex main-links">{topModelsQuery}</div>
          </section>
          {/* End Models at a glance */}
          {/* Start Form Box */}
          <section className=" col-md-11 mx-auto mt-3 py-3 pl-4 brands-form-box">
            <h4 className="title">
              Find your dream car now. From over 2.4 million vehicles in
              Europe's largest online car market.
            </h4>
            <div className="col-md-12 d-flex justify-content-center">
              <FormBox />
            </div>
          </section>
          {/* End Form Box */}
          {/* Start Full Describe */}
          <section className=" col-md-11 mx-auto mt-5 py-3 pl-4 brands-full-describe" id="tofff">
            <div className="col-md-8 brands-full-describe-box mx-auto mb-4">
              <h4>
                Peculiarities of the{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                car brand
              </h4>
              <ul>
                <li>
                  <span className="brands-full-describe-brand-style">
                    {brand}
                  </span>{" "}
                  is an automobile manufacturer of sports cars and Formula 1
                  vehicles.
                </li>
                <li>
                  The car brand has a successful motorsport division that has
                  won both the Formula 1 World Championship and the 24 Hours of
                  Le Mans multiple times.
                </li>
                <li>
                  <span className="brands-full-describe-brand-style">
                    {brand}
                  </span>{" "}
                  cars are usually only available to order, and waiting lists
                  can be several years.
                </li>
                <li>
                  <span className="brands-full-describe-brand-style">
                    {brand}
                  </span>{" "}
                  is a public company and the cars are sold in over 60 countries
                  around the world.
                </li>
              </ul>
            </div>
            <div className="col-md-8 brands-full-describe-box mx-auto mb-4">
              <h4>brand history</h4>
              <p>
                The origin of{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                goes back to the Scuderia{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                racing team. From 1929 to 1938, it drove under the direction of
                Enzo
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>
                , among other things as the Alfa Romeo works team, in successful
                car races. In 1940 the Scuderia was renamed 'Auto Avio
                Costruzioni{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>
                ' and in 1943 it moved to Maranello, where it still has its
                headquarters today. The first{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>
                , the 125C Sport, was built in 1947. At that time,{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>
                primarily developed racing cars for sports car racing . Later,
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                also produced road cars that were not suitable for racing.
                Today,{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                continues to make sporty car models and is a public company.
              </p>
            </div>
            <div className="col-md-8 brands-full-describe-box mx-auto mb-4">
              <h4>models and variants</h4>
              <p>
                All{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                models are highly motorized as six-cylinder, eight-cylinder and
                twelve-cylinder. These include e.g. B. the model{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                296,{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                F8 and the{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                Monza SP.{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>
                also offers super sports cars, such as the{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                SF90. Another model that will be available from 2022 and
                represents{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>
                's entry into the SUV segment is the{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                Purosangue.
              </p>
            </div>
            <div className="col-md-8 brands-full-describe-box mx-auto mb-4">
              <h4>
                Model overview of the car brand{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                2023 :
              </h4>
              <div className="col-md-12 table-box px-0">
                <table className="table table-light table-striped table-hover responsive">
                  <thead>
                    <tr>
                      <th scope="col">Model</th>
                      <th scope="col">Motorization</th>
                      <th scope="col">Performance</th>
                      <th scope="col">Top Speed</th>
                      <th scope="col">Acceleration 0-100km/h</th>
                      <th scope="col">Tare</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <span className="brands-full-describe-brand-style">
                          {brand}
                        </span>{" "}
                        Purosangue
                      </td>
                      <td>petrol</td>
                      <td>533 kW (725 hp)</td>
                      <td>312km/h</td>
                      <td>3.3s</td>
                      <td>2,033kg</td>
                    </tr>
                    <tr>
                      <td>
                        <span className="brands-full-describe-brand-style">
                          {brand}
                        </span>{" "}
                        812
                      </td>
                      <td>petrol</td>
                      <td>588-610 kW (800-830 hp)</td>
                      <td>240km/h</td>
                      <td>2.8s</td>
                      <td>1,588-1,630kg</td>
                    </tr>
                    <tr>
                      <td>
                        <span className="brands-full-describe-brand-style">
                          {brand}
                        </span>{" "}
                        296
                      </td>
                      <td>petrol, electric</td>
                      <td>610 kW (830 hp)</td>
                      <td>330km/h</td>
                      <td>2.9s</td>
                      <td>1,470-1,540kg</td>
                    </tr>
                    <tr>
                      <td>
                        <span className="brands-full-describe-brand-style">
                          {brand}
                        </span>{" "}
                        SF90
                      </td>
                      <td>petrol, electric</td>
                      <td>735 kW (1000 hp)</td>
                      <td>340km/h</td>
                      <td>2.5s</td>
                      <td>1,570-1,670kg</td>
                    </tr>
                    <tr>
                      <td>
                        <span className="brands-full-describe-brand-style">
                          {brand}
                        </span>{" "}
                        F8
                      </td>
                      <td>petrol</td>
                      <td>530 kW (720 hp)</td>
                      <td>340km/h</td>
                      <td>2.9s</td>
                      <td>1,435kg</td>
                    </tr>
                    <tr>
                      <td>
                        <span className="brands-full-describe-brand-style">
                          {brand}
                        </span>{" "}
                        Roma
                      </td>
                      <td>petrol</td>
                      <td>456 kW (620 hp)</td>
                      <td>320km/h</td>
                      <td>3.4s</td>
                      <td>1,570kg</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="col-md-8 brands-price mx-auto mb-4">
              <h4>Prices</h4>
              <p>
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                car prices vary by model and variant. The cheaper sports cars
                with a price from around 200,000 euros include z. the
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                Roma ,{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                F8 ,{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                296 and{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                Portofino M. Variants that are in the middle price range are
                e.g. B. the SUV
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                Purosangue with a base price of around 300,000 euros or the{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                SF90 , which can be bought from around 418,000 euros. The most
                expensive models include the{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                Daytona with a price starting at around 1.9 million euros and
                the
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                Monza with an entry-level price starting at around 3 million
                euros.
              </p>
              <img
                src={`/assets/images/cars/${brand}/singleBrand.jpg`}
                className="col-md-12"
              />
              <h4 className="mt-5">used car prices</h4>
              <p>
                The used car prices of{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                models are higher compared to other car brands. The price of a{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                Portofino M used is around 215,000 euros, that of a{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                Roma around 250,000 euros. The car model{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                812 can e.g. B. can be bought from around 383,000 euros and a{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>{" "}
                Spider is 2022 used not less than 360,000 euros for sale.
              </p>
              <h4 className="mt-5">alternatives</h4>
              <p>
                There are a few car brands that offer sporty alternatives to
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>
                , including Lamborghini, Aston Martin and McLaren. Each of these
                brands has a unique design and performance-oriented driving
                characteristics. Lamborghini is known for its luxurious and
                exotic design. Aston Martin stands for elegance and performance
                and McLaren for speed and agility. As a result, these car brands
                offer vehicles that are comparable to{" "}
                <span className="brands-full-describe-brand-style">
                  {brand}
                </span>
                in terms of style, performance and price.
              </p>
            </div>
          </section>
          {/* End Full Describe */}
          {/* Start FAQ */}
          <section className=" col-md-11 mx-auto mt-5 py-3 pl-4 brands-full-describe">
            <div className="col-md-8 brands-full-describe-box mx-auto mb-4">
              <h4 className="text-center">FAQ</h4>
              <div
                className="accordion accordion-flush"
                id="accordionFlushExample"
              >
                <div className="accordion-item">
                  <h2 className="accordion-header" id="flush-headingOne">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#flush-collapseOne"
                      aria-expanded="false"
                      aria-controls="flush-collapseOne"
                    >
                      Why is there a horse on the{" "}
                      <span className="brands-full-describe-brand-style pl-1">
                        {" "}
                        {brand}
                      </span>
                    </button>
                  </h2>
                  <div
                    id="flush-collapseOne"
                    className="accordion-collapse collapse"
                    aria-labelledby="flush-headingOne"
                    data-bs-parent="#accordionFlushExample"
                  >
                    <div className="accordion-body">
                      Today, the{" "}
                      <span className="brands-full-describe-brand-style">
                        {" "}
                        {brand}
                      </span>{" "}
                      logo is a symbol of luxury and excellence, as well as
                      speed and power. There are a few theories as to why a
                      horse is featured on the{" "}
                      <span className="brands-full-describe-brand-style">
                        {" "}
                        {brand}
                      </span>
                      logo. One theory is that the horse originally came as a
                      constant symbol of{" "}
                      <span className="brands-full-describe-brand-style">
                        {" "}
                        {brand}
                      </span>
                      's speed and power. Another possibility is that the horse
                      should represent the company's competitive spirit. Another
                      theory goes further into the story and relates to the
                      military unit in which Enzo{" "}
                      <span className="brands-full-describe-brand-style">
                        {" "}
                        {brand}
                      </span>
                      's brother fought and died in World War I. This also had
                      the jumping horse in its coat of arms.
                    </div>
                  </div>
                </div>
                <div className="accordion-item">
                  <h2 className="accordion-header" id="flush-headingTwo">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#flush-collapseTwo"
                      aria-expanded="false"
                      aria-controls="flush-collapseTwo"
                    >
                      What is the best selling{" "}
                      <span className="brands-full-describe-brand-style pl-1">
                        {" "}
                        {brand}
                      </span>
                      ?
                    </button>
                  </h2>
                  <div
                    id="flush-collapseTwo"
                    className="accordion-collapse collapse"
                    aria-labelledby="flush-headingTwo"
                    data-bs-parent="#accordionFlushExample"
                  >
                    <div className="accordion-body">
                      The best-selling{" "}
                      <span className="brands-full-describe-brand-style pl-1">
                        {" "}
                        {brand}
                      </span>{" "}
                      to date is the **
                      <span className="brands-full-describe-brand-style pl-1">
                        {" "}
                        {brand}
                      </span>{" "}
                      360** sports car.
                    </div>
                  </div>
                </div>

                <div className="accordion-item">
                  <h2 className="accordion-header" id="flush-headingThree">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#flush-collapseThree"
                      aria-expanded="false"
                      aria-controls="flush-collapseThree"
                    >
                      Where does{" "}
                      <span className="brands-full-describe-brand-style pl-1">
                        {" "}
                        {brand}
                      </span>{" "}
                      come from?
                    </button>
                  </h2>
                  <div
                    id="flush-collapseThree"
                    className="accordion-collapse collapse"
                    aria-labelledby="flush-headingThree"
                    data-bs-parent="#accordionFlushExample"
                  >
                    <div className="accordion-body">
                      <span className="brands-full-describe-brand-style pl-1">
                        {" "}
                        {brand}
                      </span>{" "}
                      was named after its founder Enzo{" "}
                      <span className="brands-full-describe-brand-style pl-1">
                        {" "}
                        {brand}
                      </span>{" "}
                      and is based in Maranello, Italy.
                    </div>
                  </div>
                </div>

                <div className="accordion-item">
                  <h2 className="accordion-header" id="flush-headingFour">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#flush-collapseFour"
                      aria-expanded="false"
                      aria-controls="flush-collapseFour"
                    >
                      Who Invented{" "}
                      <span className="brands-full-describe-brand-style pl-1">
                        {brand}
                      </span>
                      ?
                    </button>
                  </h2>
                  <div
                    id="flush-collapseFour"
                    className="accordion-collapse collapse"
                    aria-labelledby="flush-headingFour"
                    data-bs-parent="#accordionFlushExample"
                  >
                    <div className="accordion-body">
                      Former racing driver Enzo
                      <span className="brands-full-describe-brand-style pl-1">
                        {brand}
                      </span>{" "}
                      is the founder of the{" "}
                      <span className="brands-full-describe-brand-style pl-1">
                        {brand}
                      </span>{" "}
                      car brand.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          {/* End FAQ */}
        </div>
      </Layout>
    </>
  );
}
