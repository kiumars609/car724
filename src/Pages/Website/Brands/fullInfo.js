export function fullInfo(brand) {
  // console.log(brand);

  let query = [];
  if (brand === "audi") {
    query = {
      img: "audi.png",
      name: "Audi",
      value: "audi",
      motto: "projection by technology",
      models: [
        { name: "A4", img: "model-a4.jpg" },
        { name: "A3", img: "model-a3.jpeg" },
        { name: "A6", img: "model-a6.jpg" },
        { name: "Q3", img: "model-q3.jpeg" },
        { name: "Q5", img: "model-q5.jpg" },
        { name: "A5", img: "model-a5.jpg" },
        { name: "A1", img: "model-a1.jpg" },
        { name: "Q2", img: "model-q2.jpg" },
      ],
      shortDescribe: "Audi is a German car manufacturer founded in 1909 by August Horch. Audi became part of the Volkswagen Group in 1968 and since then has manufactured a wide range of vehicles, from sedans and SUVs to sports cars and luxury cars. Audi is known for technology and performance, but also for high-quality design. The combination of performance and style has helped make Audi one of the most respected car brands on the market.",
      topModels: [
        { name: "A4" },
        { name: "R8" },
        { name: "TT" },
        { name: "e-tron" },
        { name: "RS7" },
        { name: "Q8" },
        { name: "A8" },
        { name: "RS6 Avant" },
      ],
    };
  }
  if (brand === "bmw") {
    query = {
      img: "bmw.webp",
      name: "BMW",
      value: "bmw",
      motto: "Sheer driving pleasure",
      models: [
        { name: "X Series", img: "model-x-series.jpg" },
        { name: "3 Series G20", img: "model-3-series.jpg" },
        { name: "5 Series", img: "model-5-series.jpeg" },
        { name: "1 Series", img: "model-1-series.jpeg" },
        { name: "2 Series", img: "model-2-series.jpg" },
        { name: "4 Series", img: "model-4-series.jpeg" },
        { name: "M Series", img: "model-m-series.jpg" },
        { name: "i3", img: "model-i3.jpg" },
      ],
      shortDescribe: "BMW is a German automobile manufacturer founded in 1916. The company initially manufactured aircraft engines, but began producing motorcycles in 1923 and automobiles in 1928. BMW is headquartered in Munich and sells its cars and motorcycles under the BMW, Mini and Rolls-Royce brands. In 2021, BMW sold over 2.5 million vehicles worldwide.",
      topModels: [
        { name: "M8" },
        { name: "X7" },
        { name: "XM" },
        { name: "Z4" },
        { name: "M3" },
        { name: "i7" },
        { name: "528" },
        { name: "X4" },
      ],
    };
  }
  if (brand === "mercedes-benz") {
    query = {
      img: "mercedes-benz.png",
      name: "Mercedes Benz",
      value: "mercedes-benz",
      motto: "The Best or Nothing",
      models: [
        { name: "C Class", img: "model-cclass.jpeg" },
        { name: "E Class", img: "model-eclass.jpeg" },
        { name: "A Class", img: "model-aclass.jpg" },
        { name: "GLC", img: "model-glc.jpg" },
        { name: "CLA Class", img: "model-claclass.jpg" },
        { name: "B Class", img: "model-bclass.jpeg" },
        { name: "GLE", img: "model-gle.jpg" },
        { name: "V Class", img: "model-vclass.jpg" },
      ],
      shortDescribe: "Mercedes-Benz is a German car manufacturer headquartered in Stuttgart, Baden-Württemberg. The company was founded in 1926 and specializes in the manufacture of premium and luxury vehicles. The brand is known for its wide range of vehicle types, from small cars to SUVs. In recent years, Mercedes-Benz has expanded its range to include numerous hybrid variants and electric vehicles. Many new models of the car brand will be launched in 2022. The innovations include an updated design language, advanced safety assistants and powerful electric drives.",
      topModels: [
        { name: "AMG GT" },
        { name: "CLA 45 AMG" },
        { name: "E63 AMG" },
        { name: "EQV 300" },
        { name: "G65 AMG" },
        { name: "ML 500" },
        { name: "Maybach S Class" },
        { name: "S680" },
        { name: "SLC 500" },
        { name: "V300" },
      ],
    };
  }
  if (brand === "porsche") {
    query = {
      img: "porsche.png",
      name: "Porsche",
      value: "porsche",
      motto: "There Is No Substitute",
      models: [
        { name: "911 Safari", img: "model-911-safari.jpg" },
        { name: "Cayenne", img: "model-cayenne.jpg" },
        { name: "Macan", img: "model-macan.jpg" },
        { name: "Panamera", img: "model-panamera.jpg" },
        { name: "Taycan ", img: "model-taycan.jpg" },
        { name: "Boxster", img: "model-boxster.jpg" },
        { name: "Cayman", img: "model-cayman.jpg" },
        { name: "Targa", img: "model-targa.jpg" },
        { name: "Carrera GT", img: "model-carrera-gt.jpg" },
      ],
      shortDescribe: "Porsche is a German automobile manufacturer founded in 1931 by Ferdinand Porsche. The company is best known for its sports cars, which are among the most performance-oriented and luxurious vehicles on the market. Models such as the 911, Panamera or Cayenne are very popular today. Thanks to the combination of luxury, design and performance, Porsche is one of the most renowned car brands in the world.",
      topModels: [
        { name: "Carrera GT" },
        { name: "718" },
        { name: "911" },
        { name: "Panamera" },
        { name: "Taycan" },
        { name: "968" },
        { name: "Targa" },
        { name: "Cayman" },
        { name: "Cayenne" },
        { name: "928" },
      ],
    };
  }
  if (brand === "ferrari") {
    query = {
      img: "ferrari.png",
      name: "Ferrari",
      value: "ferrari",
      motto: "We are the competition",
      models: [
        { name: "458", img: "model-458.jpg" },
        { name: "California", img: "model-california.jpeg" },
        { name: "599", img: "model-599.jpeg" },
        { name: "FF", img: "model-ff.jpg" },
        { name: "612 Scaglietti", img: "model-612-scaglietti.jpeg" },
        { name: "575", img: "model-575.jpeg" },
        { name: "456", img: "model-456.jpg" },
        { name: "Superamerica", img: "model-superamerica.jpg" },
      ],
      shortDescribe:
        "Ferrari is an Italian luxury sports car manufacturer founded in 1947 by Enzo Ferrari. The company is headquartered in Maranello, Italy and is one of the most famous and successful car manufacturers in the world. Characteristic of the car brand Ferrari is not only the red color, also called rosso corsa ('racing red'), but also the horse logo. To this day, Ferrari remains at the forefront of performance and design with a range of vehicles that combine Italian flair with cutting-edge technology. ",
      topModels: [
        { name: "458" },
        { name: "FF" },
        { name: "F430" },
        { name: "F50" },
        { name: "F40" },
        { name: "California" },
        { name: "Testarossa" },
        { name: "F12" },
        { name: "488" },
        { name: "LaFerrari" },
      ],
    };
  }
  if (brand === "lamborghini") {
    query = {
      img: "lamborghini.png",
      name: "Lamborghini",
      value: "lamborghini",
      motto:
        "We are Lamborghini. Dream, inspire, lead, wait, last: see the future and write your history.",
      models: [
        { name: "Urus", img: "model-urus.jpg" },
        { name: "Aventador", img: "model-aventador.jpeg" },
        { name: "Gallardo", img: "model-gallardo.jpg" },
        { name: "Murcielago", img: "model-murcielago.jpg" },
        { name: "Diablo", img: "model-diablo.jpg" },
      ],
      shortDescribe: "Lamborghini is an Italian automobile company founded in 1963 by Ferruccio Lamborghini. Automobili Lamborghini SpA is part of Audi AG and the Volkswagen Group. The company's headquarters are located in Sant'Agata Bolognese, Italy, and the company produces high-performance sports cars. The most famous Lamborghini model is the Countach, which was launched in 1974. Famed for its sharp edges and aggressive styling, the Countach helped establish Lamborghini as a leading manufacturer of luxury sports cars. In 2022, the company's product range will include the Huracán, Aventador and Urus.",
      topModels: [
        { name: "Aventador" },
        { name: "Sian" },
        { name: "Countach" },
        { name: "Huracan" },
        { name: "Gallardo" },
        { name: "Urus" },
        { name: "Centenario" },
        { name: "Diablo" },
        { name: "Reventon" },
      ],
    };
  }
  if (brand === "ford") {
    query = {
      img: "ford.png",
      name: "Ford",
      value: "ford",
      motto: "Ford Tough",
      models: [
        { name: "458", img: "model-458.jpg" },
        { name: "California", img: "model-california.jpeg" },
        { name: "599", img: "model-599.jpeg" },
        { name: "FF", img: "model-ff.jpg" },
        { name: "612 Scaglietti", img: "model-612-scaglietti.jpeg" },
        { name: "575", img: "model-575.jpeg" },
        { name: "456", img: "model-456.jpg" },
        { name: "Superamerica", img: "model-superamerica.jpg" },
      ],
    };
  }
  if (brand === "chevrolet") {
    query = {
      img: "chevrolet.png",
      name: "Chevrolet",
      value: "chevrolet",
      motto: "Find New Roads",
      models: [
        { name: "458", img: "model-458.jpg" },
        { name: "California", img: "model-california.jpeg" },
        { name: "599", img: "model-599.jpeg" },
        { name: "FF", img: "model-ff.jpg" },
        { name: "612 Scaglietti", img: "model-612-scaglietti.jpeg" },
        { name: "575", img: "model-575.jpeg" },
        { name: "456", img: "model-456.jpg" },
        { name: "Superamerica", img: "model-superamerica.jpg" },
      ],
    };
  }
  if (brand === "toyota") {
    query = {
      img: "toyota.webp",
      name: "Toyota",
      value: "toyota",
      motto: "Let's Go Places",
      models: [
        { name: "458", img: "model-458.jpg" },
        { name: "California", img: "model-california.jpeg" },
        { name: "599", img: "model-599.jpeg" },
        { name: "FF", img: "model-ff.jpg" },
        { name: "612 Scaglietti", img: "model-612-scaglietti.jpeg" },
        { name: "575", img: "model-575.jpeg" },
        { name: "456", img: "model-456.jpg" },
        { name: "Superamerica", img: "model-superamerica.jpg" },
      ],
    };
  }
  if (brand === "lexus") {
    query = {
      img: "lexus.png",
      name: "Lexus",
      value: "lexus",
      motto: "The Relentless Pursuit of Perfection",
      models: [
        { name: "458", img: "model-458.jpg" },
        { name: "California", img: "model-california.jpeg" },
        { name: "599", img: "model-599.jpeg" },
        { name: "FF", img: "model-ff.jpg" },
        { name: "612 Scaglietti", img: "model-612-scaglietti.jpeg" },
        { name: "575", img: "model-575.jpeg" },
        { name: "456", img: "model-456.jpg" },
        { name: "Superamerica", img: "model-superamerica.jpg" },
      ],
    };
  }
  if (brand === "nissan") {
    query = {
      img: "nissan.webp",
      name: "Nissan",
      value: "nissan",
      motto: "Innovation That Excites",
      models: [
        { name: "458", img: "model-458.jpg" },
        { name: "California", img: "model-california.jpeg" },
        { name: "599", img: "model-599.jpeg" },
        { name: "FF", img: "model-ff.jpg" },
        { name: "612 Scaglietti", img: "model-612-scaglietti.jpeg" },
        { name: "575", img: "model-575.jpeg" },
        { name: "456", img: "model-456.jpg" },
        { name: "Superamerica", img: "model-superamerica.jpg" },
      ],
    };
  }
  if (brand === "bugatti") {
    query = {
      img: "bugatti.png",
      name: "Bugatti",
      value: "bugatti",
      motto: "create the incomparable",
      models: [
        { name: "Centodieci", img: "model-458.jpg" },
        { name: "Chiron", img: "model-california.jpeg" },
        { name: "Divo", img: "model-599.jpeg" },
        { name: "EB 110", img: "model-ff.jpg" },
        { name: "EB 112", img: "model-612-scaglietti.jpeg" },
        { name: "Veyron", img: "model-575.jpeg" },
      ],
      shortDescribe: "Bugatti is a French car brand founded by Ettore Bugatti in 1909. The company is known for its luxury cars, which are among the most expensive and fastest cars on the market. Bugatti has won many races such as Examples include the 24 Hours of Le Mans and the Indianapolis 500. Today, Bugatti continues to make some of the world's most expensive and exclusive cars.",
      topModels: [
        { name: "Centodieci" },
        { name: "Divo" },
        { name: "Chiron" },
        { name: "Veyron" },
        { name: "EB 112" },
        { name: "EB 110" },
      ],
    };
  }
  if (brand === "renault") {
    query = {
      img: "renault.png",
      name: "Renault",
      value: "renault",
      motto: "create the life that goes with it",
      models: [
        { name: "458", img: "model-458.jpg" },
        { name: "California", img: "model-california.jpeg" },
        { name: "599", img: "model-599.jpeg" },
        { name: "FF", img: "model-ff.jpg" },
        { name: "612 Scaglietti", img: "model-612-scaglietti.jpeg" },
        { name: "575", img: "model-575.jpeg" },
        { name: "456", img: "model-456.jpg" },
        { name: "Superamerica", img: "model-superamerica.jpg" },
      ],
    };
  }
  if (brand === "peugeot") {
    query = {
      img: "peugeot.webp",
      name: "Peugeot",
      value: "peugeot",
      motto: "Lions of our time",
      models: [
        { name: "458", img: "model-458.jpg" },
        { name: "California", img: "model-california.jpeg" },
        { name: "599", img: "model-599.jpeg" },
        { name: "FF", img: "model-ff.jpg" },
        { name: "612 Scaglietti", img: "model-612-scaglietti.jpeg" },
        { name: "575", img: "model-575.jpeg" },
        { name: "456", img: "model-456.jpg" },
        { name: "Superamerica", img: "model-superamerica.jpg" },
      ],
    };
  }
  if (brand === "kia") {
    query = {
      img: "kia.webp",
      name: "KIA",
      value: "kia",
      motto: "Movement That Inspires",
      models: [
        { name: "458", img: "model-458.jpg" },
        { name: "California", img: "model-california.jpeg" },
        { name: "599", img: "model-599.jpeg" },
        { name: "FF", img: "model-ff.jpg" },
        { name: "612 Scaglietti", img: "model-612-scaglietti.jpeg" },
        { name: "575", img: "model-575.jpeg" },
        { name: "456", img: "model-456.jpg" },
        { name: "Superamerica", img: "model-superamerica.jpg" },
      ],
    };
  }
  if (brand === "hyundai") {
    query = {
      img: "hyundai.png",
      name: "Hyundai",
      value: "hyundai",
      motto: "New Thinking, New Possibilities",
      models: [
        { name: "458", img: "model-458.jpg" },
        { name: "California", img: "model-california.jpeg" },
        { name: "599", img: "model-599.jpeg" },
        { name: "FF", img: "model-ff.jpg" },
        { name: "612 Scaglietti", img: "model-612-scaglietti.jpeg" },
        { name: "575", img: "model-575.jpeg" },
        { name: "456", img: "model-456.jpg" },
        { name: "Superamerica", img: "model-superamerica.jpg" },
      ],
    };
  }
  if (brand === "volkswagen") {
    query = {
      img: "volkswagen.png",
      name: "Volkswagen",
      value: "volkswagen",
      motto: "Nothing else is a Volkswagen. Think small.",
      models: [
        { name: "458", img: "model-458.jpg" },
        { name: "California", img: "model-california.jpeg" },
        { name: "599", img: "model-599.jpeg" },
        { name: "FF", img: "model-ff.jpg" },
        { name: "612 Scaglietti", img: "model-612-scaglietti.jpeg" },
        { name: "575", img: "model-575.jpeg" },
        { name: "456", img: "model-456.jpg" },
        { name: "Superamerica", img: "model-superamerica.jpg" },
      ],
    };
  }
  if (brand === "more") {
    query = {
      img: "more.png",
      name: "More brands",
      value: "more",
    };
  }

  const brands = [
    {
      img: "audi.png",
      name: "Audi",
      value: "audi",
    },
    {
      img: "bmw.webp",
      name: "BMW",
      value: "bmw",
    },
    {
      img: "mercedes-benz.png",
      name: "Mercedes Benz",
      value: "mercedes-benz",
    },
    {
      img: "porsche.png",
      name: "Porsche",
      value: "porsche",
    },
    {
      img: "ferrari.png",
      name: "Ferrari",
      value: "ferrari",
    },
    {
      img: "lamborghini.png",
      name: "Lamborghini",
      value: "lamborghini",
    },
    { img: "ford.png", name: "Ford", value: "ford" },
    {
      img: "chevrolet.png",
      name: "Chevrolet",
      value: "chevrolet",
    },
    {
      img: "toyota.webp",
      name: "Toyota",
      value: "toyota",
    },
    {
      img: "lexus.png",
      name: "Lexus",
      value: "lexus",
    },
    {
      img: "nissan.webp",
      name: "Nissan",
      value: "nissan",
    },
    {
      img: "bugatti.png",
      name: "Bugatti",
      value: "bugatti",
    },
    {
      img: "renault.png",
      name: "Renault",
      value: "renault",
    },
    {
      img: "peugeot.webp",
      name: "Peugeot",
      value: "peugeot",
    },
    {
      img: "kia.webp",
      name: "KIA",
      value: "kia",
    },
    {
      img: "hyundai.png",
      name: "Hyundai",
      value: "hyundai",
    },
    {
      img: "volkswagen.png",
      name: "Volkswagen",
      value: "volkswagen",
    },
    { img: "more.png", name: "More brands", value: "more" },
  ];

  return { query, brands };
}
