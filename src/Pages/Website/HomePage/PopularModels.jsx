import React from "react";

export default function PopularModels() {
  return (
    <>
      <section className="ftco-section services-section ftco-no-pt ftco-no-pb py-5 popular-models">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-12 heading-section text-center mb-5">
              <h2 className="mb-2">Popular Models</h2>
              <span className="subheading d-none">body types</span>
            </div>
          </div>

          <div className="row d-flex">
            <div className="col-md-2 d-flex align-self-stretch">
              <div className="media block-6 services">
                <div className="media-body py-md-4 text-center">
                  <h3 className="heading mb-0">Audi</h3>
                  <a href="#">Audi A1</a>
                  <a href="#">Audi A3</a>
                  <a href="#">Audi A4</a>
                  <a href="#">Audi A6</a>
                  <a href="#">Audi Q3</a>
                </div>
              </div>
            </div>


            <div className="col-md-2 d-flex align-self-stretch">
              <div className="media block-6 services">
                <div className="media-body py-md-4 text-center">
                  <h3 className="heading mb-0">BMW</h3>
                  <a href="#">BMW 1er</a>
                  <a href="#">BMW 3er</a>
                  <a href="#">BMW 5er</a>
                  <a href="#">BMW X1</a>
                  <a href="#">BMW X3</a>
                </div>
              </div>
            </div>


            <div className="col-md-2 d-flex align-self-stretch">
              <div className="media block-6 services">
                <div className="media-body py-md-4 text-center">
                  <h3 className="heading mb-0">VW</h3>
                  <a href="#">VW Caddy</a>
                  <a href="#">VW Golf</a>
                  <a href="#">VW Polo</a>
                  <a href="#">VW Tiguan</a>
                  <a href="#">VW Touran</a>
                </div>
              </div>
            </div>


            <div className="col-md-2 d-flex align-self-stretch">
              <div className="media block-6 services">
                <div className="media-body py-md-4 text-center">
                  <h3 className="heading mb-0">Mercedes</h3>
                  <a href="#">Mercedes A-Class</a>
                  <a href="#">Mercedes B-Class</a>
                  <a href="#">Mercedes C-Class</a>
                  <a href="#">Mercedes E-Class</a>
                  <a href="#">Mercedes S-Class</a>
                </div>
              </div>
            </div>


            <div className="col-md-2 d-flex align-self-stretch">
              <div className="media block-6 services">
                <div className="media-body py-md-4 text-center">
                  <h3 className="heading mb-0">Popular Electric Cars</h3>
                  <a href="#">Audi e-tron</a>
                  <a href="#">BMW i3</a>
                  <a href="#">Renault Twingo Z.E.</a>
                  <a href="#">Tesla Model 3</a>
                  <a href="#">Tesla Model S</a>
                </div>
              </div>
            </div>


            <div className="col-md-2 d-flex align-self-stretch">
              <div className="media block-6 services">
                <div className="media-body py-md-4 text-center">
                  <h3 className="heading mb-0">Current Electric Cars</h3>
                  <a href="#">BMW i4</a>
                  <a href="#">Dacia Spring</a>
                  <a href="#">Mercedes-Benz EQE</a>
                  <a href="#">Porsche Taycan</a>
                  <a href="#">VW ID.5</a>
                </div>
              </div>
            </div>

          </div>
        </div>
      </section>
    </>
  );
}
