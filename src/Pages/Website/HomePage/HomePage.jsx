import React from "react";
import Layout from "../../../Component/Web/Layout/Layout";
import "./scss/homePage.css";
import Slider from "./Slider/Slider";
import OurServices from "./OurServices";
import Products from "./Products/Products";
import HowItWork from "./HowItWork";
import HappyCustomer from "./HappyCustomer";
import AboutUs from "./AboutUs/AboutUs";
import Blog from "./Blog";
import BodyType from "./BodyType";
import PopularBrands from "./PopularBrands";
import PopularModels from "./PopularModels";
import { Backend } from "./Products/Backend";
import Loading from "../../../Component/Loading/";
import ContactUs from "./ContactUs";

export default function HomePage() {
  const {
    data,
    loader,
    aboutUsRefScroll,
    contactUsRefScroll,
    executeAboutUsScroll,
    executeContactUsScroll,
  } = Backend();
  return (
    <>
      <Layout
        pageTitle={"Home"}
        executeAboutUsScroll={executeAboutUsScroll}
        executeContactUsScroll={executeContactUsScroll}
      >
        <Slider />
        {loader ? (
          <div
            className="col-md-12"
            style={{ height: "600px", background: "#000" }}
          >
            <Loading />
          </div>
        ) : (
          <Products loader={loader} data={data} />
        )}
        <BodyType />
        <PopularBrands />
        <PopularModels />
        <AboutUs aboutUsRefScroll={aboutUsRefScroll} />
        <ContactUs contactUsRefScroll={contactUsRefScroll} />
        {/* <OurServices /> */}
        {/* <HowItWork /> */}
        {/* <HappyCustomer /> */}
        {/* <Blog /> */}
      </Layout>
    </>
  );
}
