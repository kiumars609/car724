import React from "react";
import { Link } from "react-router-dom";
import { fullInfo } from "../Brands/fullInfo";

export default function PopularBrands() {
  const { brands } = fullInfo();

  const queryBrands =
    brands &&
    brands.map((item, index) => {
      return (
        <div
          className="col-md-2 d-flex align-self-stretch main-cards-brands mb-4"
          key={index}
        >
          <Link to={`/brands/${item.value}`}>
            <div className="media block-6 services">
              <div className="media-body text-center">
                <div className="d-flex mb-3 align-items-center justify-content-center car-icons">
                  <img
                    src={`/assets/images/logo/${item.img}`}
                    className="col-md-12"
                  />
                </div>
                <h3 className="heading mb-0">{item.name}</h3>
              </div>
            </div>
          </Link>
        </div>
      );
    });

  return (
    <>
      <section className="ftco-section services-section ftco-no-pt ftco-no-pb py-5 popular-brands">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-12 heading-section text-center mb-5">
              <h2 className="mb-2">Popular Brands</h2>
              <span className="subheading d-none">body types</span>
            </div>
          </div>
          <div className="row d-flex">{queryBrands}</div>
        </div>
      </section>
    </>
  );
}
