import React from "react";

export default function HowItWork() {
  return (
    <>
      <section
        className="ftco-section services-section img"
        style={{ backgroundImage: `url(/assets/images/bg_2.jpg)` }}
      >
        <div className="overlay"></div>
        <div className="container">
          <div className="row justify-content-center mb-5">
            <div className="col-md-7 text-center heading-section heading-section-white ftco-animate">
              <span className="subheading">Work flow</span>
              <h2 className="mb-3">How it works</h2>
            </div>
          </div>
          <div className="row">
            <div className="col-md-3 d-flex align-self-stretch ftco-animate">
              <div className="media block-6 services services-2">
                <div className="media-body py-md-4 text-center">
                  <div className="icon d-flex align-items-center justify-content-center">
                    <span className="flaticon-route"></span>
                  </div>
                  <h3>Pick Destination</h3>
                  <p>
                    A small river named Duden flows by their place and supplies
                    it with you
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-3 d-flex align-self-stretch ftco-animate">
              <div className="media block-6 services services-2">
                <div className="media-body py-md-4 text-center">
                  <div className="icon d-flex align-items-center justify-content-center">
                    <span className="flaticon-select"></span>
                  </div>
                  <h3>Select Term</h3>
                  <p>
                    A small river named Duden flows by their place and supplies
                    it with you
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-3 d-flex align-self-stretch ftco-animate">
              <div className="media block-6 services services-2">
                <div className="media-body py-md-4 text-center">
                  <div className="icon d-flex align-items-center justify-content-center">
                    <span className="flaticon-rent"></span>
                  </div>
                  <h3>Choose A Car</h3>
                  <p>
                    A small river named Duden flows by their place and supplies
                    it with you
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-3 d-flex align-self-stretch ftco-animate">
              <div className="media block-6 services services-2">
                <div className="media-body py-md-4 text-center">
                  <div className="icon d-flex align-items-center justify-content-center">
                    <span className="flaticon-review"></span>
                  </div>
                  <h3>Enjoy The Ride</h3>
                  <p>
                    A small river named Duden flows by their place and supplies
                    it with you
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
