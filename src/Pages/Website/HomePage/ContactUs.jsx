import React, { useState, useEffect } from "react";
import { post } from "../../../services/index";

export default function ContactUs({ contactUsRefScroll }) {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [success, setSuccess] = useState(false);
  const [nullInput, setNullInput] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (name !== "" && email !== "" && message !== "") {
      const payload = {
        name: name,
        email: email,
        message: message,
      };

      post("/contact/postContactAPI.php", JSON.stringify(payload))
        .then((res) => {})
        .then(function(response) {
          setName("");
          setEmail("");
          setMessage("");
          setSuccess(true);
          setNullInput(false);
          // toast['success']('Your Comment Inserted Successfully !!')
        });
    } else {
      setNullInput(true);
      // toast['error']('Please fill all of the fields.')
    }
  };

  return (
    <>
      <section
        className="ftco-section ftco-no-pt ftco-no-pb  col-md-12 contact-us-site"
        ref={contactUsRefScroll}
      >
        <div className="container">
          <img
            src="/assets/images/contact.webp"
            className="contact-us-site-image"
          />
          <div className="col-md-4 contact-us-site-form">
            <h5 className="text-center title">Contact Us</h5>
            <form onSubmit={handleSubmit}>
              <input
                type="text"
                className={`input col-md-12 ${nullInput && "nullll"}`}
                placeholder="Enter your Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
              <input
                type="email"
                className={`input col-md-12 ${nullInput && "nullll"}`}
                placeholder="Enter a valid email address"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <textarea
                className={`textareaa col-md-12 ${nullInput && "nullll"}`}
                placeholder="Enter your message"
                value={message}
                onChange={(e) => setMessage(e.target.value)}
              ></textarea>

              {!success ? (
                <button type="submit" className="button col-md-10">
                  Submit
                </button>
              ) : (
                <button
                  type="submit"
                  className="button success col-md-10"
                  disabled="disabled"
                >
                  Your Message Submitted Successfully !!
                </button>
              )}
            </form>
          </div>
          <div className="col-md-12 d-flex bg-danger position-relative">
            <div className="bottom-section">
              <div className="bottom-section-box">
                <span className="header">
                  <i className="fa fa-phone"></i>
                  <span> call us</span>
                </span>
                <p>+98 (915) 5042-678</p>
                <p>+98 (905) 5212-139</p>
              </div>
              <div className="bottom-section-box">
                <span className="header">
                  <i className="fa fa-location"></i>
                  <span> location</span>
                </span>
                <p>23 Sayad Shirazi St., Mashhad, Iran</p>
              </div>
              <div className="bottom-section-box">
                <span className="header">
                  <i className="fa fa-clock"></i>
                  <span> hours</span>
                </span>
                <p>Mon-Fri .... 11AM - 8PM, Sat, Sun .... 6AM - 8PM</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
