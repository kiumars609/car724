import React from "react";
import Row from "./Row";
import data from "./data";

export default function AboutUs({ aboutUsRefScroll }) {
  const getData = data;

  const selected =
    getData &&
    getData.map((item, index) => {
      return <Row key={index} value={item} side={item.side} />;
    });

  return (
    <>
      <section
        className="ftco-section ftco-no-pt ftco-no-pb my-5 about-site"
        ref={aboutUsRefScroll}
      >
        <div className="container">
          <h3 className="mb-3">
            Find your perfect car with AutoRoad724, Europe's largest online car
            market.
          </h3>
          {selected}
        </div>
      </section>
    </>
  );
}
