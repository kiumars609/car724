import React from "react";

export default function Row({ value, side }) {
  return (
    <>
      {side === "left" ? (
        <div className="row no-gutters">
          <div
            className="col-md-4 p-md-5 img img-2 d-flex justify-content-center align-items-center"
            style={{
              backgroundImage: `url(${value.image})`,
            }}
          ></div>
          <div className="col-md-8 wrap-about py-md-5">
            <div className="heading-section mb-5 pl-md-5">
              <span className="subheading">{value.title}</span>
              <h2 className="mb-4 d-none">Choose A Perfect Car</h2>
              <p>{value.text}</p>
            </div>
          </div>
        </div>
      ) : (
        <div className="row no-gutters">
          <div className="col-md-8 wrap-about py-md-5">
            <div className="heading-section mb-5 pr-md-5">
              <span className="subheading">{value.title}</span>
              <h2 className="mb-4 d-none">Choose A Perfect Car</h2>
              <p>{value.text}</p>
            </div>
          </div>
          <div
            className="col-md-4 p-md-5 img img-2 d-flex justify-content-center align-items-center"
            style={{
              backgroundImage: `url(${value.image})`,
            }}
          ></div>
        </div>
      )}
    </>
  );
}
