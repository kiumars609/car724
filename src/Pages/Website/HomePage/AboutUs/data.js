const data = [
    {
      image:
        "https://www.autoscout24.de/assets/as24-home/images/seo/content/used-car-search.jpg.webp",
      title: "ARE YOU LOOKING FOR A USED CAR?",
      text:
        "AutoRoad724 is the perfect online marketplace to start your vehicle search. Find the right new or used car for you among hundreds of thousands of offers . Regardless of whether you are looking for an economical small car, a large family SUV or a sporty coupé – you will find what you are looking for at AutoRoad724. Start your new or used car search at AutoRoad724 now.",
      side: "left",
    },
    {
      image:
        "https://www.autoscout24.de/assets/as24-home/images/seo/content/car-selling.jpg.webp",
      title: "You want to sell your car?",
      text:
        "AutoRoad724 is Europe's largest online car marketplace and helps you get the best price for your car. With the help of the AutoRoad724 marketplace, you can reach millions of potential buyers or get the best price through direct sales through a reputable dealer in your area. With over 40 million users per month, you are sure to find a buyer for your car quickly and easily. Register your vehicle at AutoRoad724 now.",
      side: "right",
    },
    {
      image:
        "https://www.autoscout24.de/assets/as24-home/images/seo/content/leasing.jpg.webp",
      title: "Would you like to lease a car?",
      text:
        "AutoRoad724 is the perfect platform for leasing a car. We work with leading leasing companies to bring you the best deals on the latest models . Whether you're looking for a family SUV or an electric car, we'll help you find the perfect leasing deal. Lease your vehicle from AutoRoad724 now.",
      side: "left",
    },
    {
      image:
        "https://www.autoscout24.de/assets/as24-home/images/seo/content/auto-abo.jpg.webp",
      title: "Do you want to take out a car subscription?",
      text:
        "A car subscription is easy and convenient for anyone who loves cars but doesn't want to own a car. With the AutoRoad724 car subscription, you can choose from a large selection of offers and stay flexible over the long term thanks to short contract periods. While you enjoy driving, our selected partners take care of maintenance, wear and tear, insurance and road tax. You just have to fill up. Subscribe now to your new car at AutoRoad724.",
      side: "right",
    },
    {
      image:
        "https://www.autoscout24.de/assets/as24-home/images/seo/content/online-purchase.jpg.webp",
      title: "Are you a fan of buying a car online?",
      text:
        "Browse online through a huge range of vehicles and have your dream car delivered to your home. With AutoRoad724 smyle you will find a high-quality, technically tested car at a fair price. We work with reputable car dealers throughout Germany and offer you the right financing, a 14-day right of return and optional extended guarantees. You can find your perfect new or used car in no time at all. Start your search for your dream car now with AutoRoad724 smyle.",
      side: "left",
    },
    {
      image:
        "https://www.autoscout24.de/assets/as24-home/images/seo/content/advice.jpg.webp",
      title: "Are you looking for information on how to make a decision when buying a car?",
      text:
        "Buying a car is a big decision and at the same time a matter of trust. Choosing the right car model, the right financing and the cheapest insurance play an important role. Our guide will help you to find the right car for you. Find out now in the AutoRoad724 magazine - the first point of contact and reliable when it comes to information about buying and selling cars. In addition to current car test reports, you will also find a wide range of advice topics there : from technology tips and lists of accessories to care recommendations and car comparisons. AutoRoad724 supports you from A to Z on the topic of cars.",
      side: "right",
    }
  ];

  export default data;