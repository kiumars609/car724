import React, { useState, useEffect, useRef } from "react";
import { get } from "../../../../services/index";

export function Backend() {
  const [data, setData] = useState([]);
  const [loader, setLoader] = useState(true);

  useEffect(() => {
    get("/cars/getCarsAPI.php").then((response) => {
      setData(response);
      setLoader(false);
    });
  }, []);

  // Scroll To About Us
  const aboutUsRefScroll = useRef(null);
  const executeAboutUsScroll = () => aboutUsRefScroll.current.scrollIntoView();

  // Scroll To Contact Us
  const contactUsRefScroll = useRef(null);
  const executeContactUsScroll = () =>
    contactUsRefScroll.current.scrollIntoView();

  return {
    data,
    loader,
    aboutUsRefScroll,
    contactUsRefScroll,
    executeAboutUsScroll,
    executeContactUsScroll,
  };
}
