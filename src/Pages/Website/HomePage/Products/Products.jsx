import React from "react";
import { useCommaSeparators } from "../../../../CustomHooks/useCommaSeparators";
import { logoes } from "./logoes";
import { Link } from "react-router-dom";

export default function Products({ data, loader }) {
  const query =
    data &&
    data.slice(0, 8).map((item) => {
      return (
        <>
          <div className="col-md-3" key={item.id}>
            <Link to={`/car-detail/${item.id}`}>
              <div className="car-wrap ">
                <div
                  className="img d-flex align-items-end"
                  style={{
                    backgroundImage: `url(/assets/images/cars/${item.brand}/${item.imageMain})`,
                  }}
                >
                  <div className="price-wrap d-flex">
                    <span className="rate">
                      €{useCommaSeparators(item.price)}
                    </span>
                  </div>
                </div>
                <div className="text p-4 text-left">
                  <span className="mb-0">{logoes(item.brand)}</span>
                  <h2 className="mb-0">
                    <a href="#">{item.shortTitle}</a>
                  </h2>
                  <span className="mb-3">
                    {useCommaSeparators(item.mileage)} km,{" "}
                    {item.firstRegistration}
                  </span>
                  <span className="mb-0">
                    <i className="fas fa-location"></i>
                    {/* <i className="fas fa-map-marker"></i> */}
                    DE 59067 Manheim
                  </span>

                  {/* <p className="d-flex mb-0 d-block">
                    <a
                      href="#"
                      className="btn btn-black btn-outline-black mr-1"
                    >
                      Book now
                    </a>{" "}
                    <a
                      href="#"
                      className="btn btn-black btn-outline-black ml-1"
                    >
                      Details
                    </a>
                  </p> */}
                </div>
              </div>
            </Link>
          </div>
        </>
      );
    });

  return (
    <>
      <section className="ftco-section">
        <div className="container-fluid px-4">
          <div className="row justify-content-center">
            <div className="col-md-12 heading-section text-center mb-5">
              <span className="subheading">What we offer</span>
              <h2 className="mb-2">Choose Your Car</h2>
            </div>
          </div>
          <div className="row">{query}</div>
        </div>
      </section>
    </>
  );
}
