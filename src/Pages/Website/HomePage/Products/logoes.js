import React from "react";

export function logoes( brand, width = 'auto' ) {
  if (brand === "mercedes-benz") {
    return <img src={`/assets/images/logo/${brand}.png`} width={width} />;
  }
  if (brand === "bmw") {
    return <img src={`/assets/images/logo/${brand}.webp`} width={width} />;
  }
  if (brand === "porsche") {
    return <img src={`/assets/images/logo/${brand}.png`} width={width} />;
  }
  if (brand === "audi") {
    return <img src={`/assets/images/logo/${brand}.png`} width={width} />;
  }
  if (brand === "lamborghini") {
    return <img src={`/assets/images/logo/${brand}.png`} width={width} />;
  }
}
