import React from "react";
import { Link } from "react-router-dom";

export default function BodyType() {
  return (
    <>
      <section className="ftco-section services-section ftco-no-pt ftco-no-pb pb-5">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-12 heading-section text-center mb-5">
              <h2 className="mb-2">
                Discover what AutoRoad724 has to offer here
              </h2>
              <span className="subheading">body types</span>
            </div>
          </div>
          <div className="row d-flex">
            <div className="col-md-2 d-flex align-self-stretch">
              <div className="media block-6 services">
                <Link to={"/body-type/coupe"}>
                  <div className="media-body py-md-4 text-center">
                    <div className="d-flex mb-3 align-items-center justify-content-center">
                      <img src="/assets/images/icon/coupeType.png" className="col-md-8 mt-3 mb-2" />
                    </div>
                    <h3 className="heading mb-0">Coupe</h3>
                  </div>
                </Link>
              </div>
            </div>

            <div className="col-md-2 d-flex align-self-stretch">
              <div className="media block-6 services">
                <Link to={"/body-type/small-car"}>
                  <div className="media-body py-md-4 text-center">
                    <div className="d-flex mb-3 align-items-center justify-content-center">
                      <svg
                        className="col-md-6"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                      >
                        <path
                          fill="currentColor"
                          fill-rule="evenodd"
                          d="M12.8 4c1 0 2 .4 2.8 1.2l5.2 5.2c.8.8 1.2 1.8 1.2 2.8V16c0 1.1-.9 2-2 2h-1.2c-.4 1.2-1.5 2-2.8 2-1.3 0-2.4-.8-2.8-2h-2.4c-.4 1.1-1.5 2-2.8 2-1.3 0-2.4-.8-2.8-2H4c-1.1 0-2-.9-2-2v-6c0-3.3 2.7-6 6-6zM8 16c-.6 0-1 .4-1 1s.4 1 1 1 1-.4 1-1-.4-1-1-1zm8 0c-.6 0-1 .4-1 1s.4 1 1 1 1-.4 1-1-.4-1-1-1zM12.8 6H8C6.1 6 4.5 7.3 4.1 9H7c.6 0 1 .4 1 1s-.4 1-1 1H4v5h1.2c.4-1.2 1.5-2 2.8-2 1.3 0 2.4.8 2.8 2h2.4c.4-1.1 1.5-2 2.8-2 1.3 0 2.4.8 2.8 2H20v-2.8c0-.5-.2-1-.6-1.4l-5.2-5.2c-.4-.4-.9-.6-1.4-.6zm.2 2c.6 0 1 .4 1 1v1c0 .6-.4 1-1 1s-1-.4-1-1V9c0-.6.4-1 1-1z"
                        ></path>
                      </svg>
                    </div>
                    <h3 className="heading mb-0">Small Car</h3>
                  </div>
                </Link>
              </div>
            </div>

            <div className="col-md-2 d-flex align-self-stretch">
              <div className="media block-6 services">
                <Link to={"/body-type/limousine"}>
                  <div className="media-body py-md-4 text-center">
                    <div className="d-flex mb-3 align-items-center justify-content-center">
                      <svg
                        className="col-md-6"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                      >
                        <path
                          fill="currentColor"
                          fill-rule="evenodd"
                          d="M12.6 5c.5 0 1 .2 1.4.6L17.4 9h1.2c1.2 0 2.3.5 3.1 1.3.8.8 1.3 1.9 1.3 3.1V15c0 1.1-.9 2-2 2h-1.2c-.4 1.2-1.5 2-2.8 2-1.3 0-2.4-.8-2.8-2H9.8c-.4 1.2-1.5 2-2.8 2-1.3 0-2.4-.8-2.8-2H3c-1.1 0-2-.9-2-2v-3.6c0-.5.2-1 .6-1.4l3.8-3.8C6.2 5.4 7.2 5 8.2 5zM7 15c-.6 0-1 .4-1 1s.4 1 1 1 1-.4 1-1-.4-1-1-1zm10 0c-.6 0-1 .4-1 1s.4 1 1 1 1-.4 1-1-.4-1-1-1zm-4.5-8H8.2c-.5 0-1 .2-1.4.6L3 11.4V15h1.2c.4-1.2 1.5-2 2.8-2 1.3 0 2.4.8 2.8 2h4.4c.4-1.2 1.5-2 2.8-2 1.3 0 2.4.8 2.8 2h1.1v-1.6c0-.6-.3-1.3-.7-1.7-.4-.4-1.1-.7-1.7-.7h-1.6c-.3 0-.5-.1-.7-.3L12.5 7zM12 8c.6 0 1 .4 1 1v1c0 .6-.4 1-1 1s-1-.4-1-1V9c0-.6.4-1 1-1zM8 9c.6 0 1 .4 1 1s-.4 1-1 1H7c-.6 0-1-.4-1-1s.4-1 1-1z"
                        ></path>
                      </svg>
                    </div>
                    <h3 className="heading mb-0">Limousine</h3>
                  </div>
                </Link>
              </div>
            </div>

            <div className="col-md-2 d-flex align-self-stretch">
              <div className="media block-6 services">
                <Link to={"/body-type/pickup"}>
                  <div className="media-body py-md-4 text-center">
                    <div className="d-flex mb-3 align-items-center justify-content-center">
                      <svg
                        className="col-md-6"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                      >
                        <path
                          fill="currentColor"
                          fill-rule="evenodd"
                          d="M10 5v5H1v5c0 1.1.9 2 2 2 0 1.7 1.3 3 3 3s3-1.3 3-3-1.3-3-3-3c-.9 0-1.7.4-2.2 1H3v-3h18v3h-.8c-.5-.6-1.3-1-2.2-1-.9 0-1.7.4-2.2 1h-3.4l-1.7-1.7c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l2 2c.2.2.4.3.7.3h3c0 1.7 1.3 3 3 3s3-1.3 3-3h1c.6 0 1-.4 1-1v-5c0-.6-.4-1-1-1h-3.4l-2.7-4.5c-.2-.3-.6-.5-.9-.5zm2 2h2.4l1.8 3H12zm-6 9c.6 0 1 .4 1 1s-.4 1-1 1-1-.4-1-1 .4-1 1-1zm12 0c.6 0 1 .4 1 1s-.4 1-1 1-1-.4-1-1 .4-1 1-1z"
                        ></path>
                      </svg>
                    </div>
                    <h3 className="heading mb-0">Pick-up</h3>
                  </div>
                </Link>
              </div>
            </div>

            <div className="col-md-2 d-flex align-self-stretch">
              <div className="media block-6 services">
                <Link to={"/body-type/suv"}>
                  <div className="media-body py-md-4 text-center">
                    <div className="d-flex mb-3 align-items-center justify-content-center">
                      <svg
                        className="col-md-6"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                      >
                        <path
                          fill="currentColor"
                          fill-rule="evenodd"
                          d="M23 11c0-.6-.4-1-1-1h-3.4l-2.7-4.5c-.2-.3-.6-.5-.9-.5H2c-.6 0-1 .4-1 1v9c0 1.1.9 2 2 2 0 1.7 1.3 3 3 3s3-1.3 3-3-1.3-3-3-3c-.9 0-1.7.4-2.2 1H3v-3h18v3h-.8c-.5-.6-1.3-1-2.2-1-.9 0-1.7.4-2.2 1h-3.4l-1.7-1.7c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l2 2c.2.2.4.3.7.3h3c0 1.7 1.3 3 3 3s3-1.3 3-3h1c.6 0 1-.4 1-1v-5zM6 16c.6 0 1 .4 1 1s-.4 1-1 1-1-.4-1-1 .4-1 1-1zm10.2-6H12V7h2.4l1.8 3zM10 7v3H3V7h7zm8 11c-.6 0-1-.4-1-1s.4-1 1-1 1 .4 1 1-.4 1-1 1z"
                        ></path>
                      </svg>
                    </div>
                    <h3 className="heading mb-0">SUV</h3>
                  </div>
                </Link>
              </div>
            </div>

            <div className="col-md-2 d-flex align-self-stretch">
              <div className="media block-6 services">
                <Link to={"/body-type/transporter"}>
                  <div className="media-body py-md-4 text-center">
                    <div className="d-flex mb-3 align-items-center justify-content-center">
                      <svg
                        className="col-md-6"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                      >
                        <path
                          fill="currentColor"
                          fill-rule="evenodd"
                          d="M22.43 8.773l-1.698-2.831A4.019 4.019 0 0017.302 4H3c-1.103 0-2 .897-2 2v11c0 1.103.897 2 2 2h1.184A2.996 2.996 0 007 21a2.996 2.996 0 002.816-2h4.369a2.996 2.996 0 002.816 2 2.996 2.996 0 002.816-2H21c1.103 0 2-.897 2-2v-6.169c0-.726-.197-1.437-.57-2.058zM17.302 6c.698 0 1.355.372 1.715.971l1.698 2.832c.037.062.055.132.085.197H16V6h1.302zM3 6h11v4h-2V9a1 1 0 10-2 0v1H7V9a1 1 0 10-2 0v1H3V6zm4 13a1.001 1.001 0 010-2 1.001 1.001 0 010 2zm10 0a1.001 1.001 0 010-2 1.001 1.001 0 010 2zm4-2h-1.184A2.996 2.996 0 0017 15a2.996 2.996 0 00-2.816 2H9.816A2.996 2.996 0 007 15a2.996 2.996 0 00-2.816 2H3v-5h18v5z"
                        ></path>
                      </svg>
                    </div>
                    <h3 className="heading mb-0">Transporter</h3>
                  </div>
                </Link>
              </div>
            </div>

            <div className="col-md-2 d-flex align-self-stretch">
              <div className="media block-6 services">
                <Link to={"/body-type/more"}>
                  <div className="media-body py-md-4 text-center">
                    <div className="d-flex mb-3 align-items-center justify-content-center">
                      <svg
                        className="col-md-6"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                      >
                        <path
                          fill="currentColor"
                          fill-rule="evenodd"
                          d="M10.8 2c1 0 2 .4 2.8 1.2l1.2 1.2c.8.7 1.2 1.7 1.2 2.8v.9c.4 0 .7.2 1 .5l2.4 2.4c2 0 3.6 1.6 3.6 3.6V16c0 1.1-.9 2-2 2h-1c0 .6-.4 1-1 1s-1-.4-1-1h-1v1c0 1.1-.9 2-2 2h-1c0 .6-.4 1-1 1s-1-.4-1-1H7c0 .6-.4 1-1 1s-1-.4-1-1H3c-.6 0-1-.4-1-1v-4.6c0-.5.2-1 .6-1.4l1-1H2c-.6 0-1-.4-1-1V4c0-1.1.9-2 2-2zM9.6 13H6.4L4 15.4V19h1c0-.6.4-1 1-1s1 .4 1 1h5c0-.6.4-1 1-1s1 .4 1 1h1v-1.4c0-.9-.7-1.6-1.6-1.6-.5 0-1-.2-1.4-.6L9.6 13zm6-3h-3.2l-1.5 1.5.016.016.068.068.016.016 2.4 2.4c1.4 0 2.6.8 3.2 2H18c0-.6.4-1 1-1s1 .4 1 1h1v-1.4c0-.9-.7-1.6-1.6-1.6-.5 0-1-.2-1.4-.6L15.6 10zM9 14c.6 0 1 .4 1 1s-.4 1-1 1H8c-.6 0-1-.4-1-1s.4-1 1-1zm6-3c.6 0 1 .4 1 1s-.4 1-1 1h-1c-.6 0-1-.4-1-1s.4-1 1-1zm-4.2-7H3v7h2c0-.6.4-1 1-1s1 .4 1 1h1.6L11 8.6c.4-.4.9-.6 1.4-.6H14v-.8c0-.5-.2-1-.6-1.4l-1.2-1.2c-.4-.4-.9-.6-1.4-.6zm.2 1c.6 0 1 .4 1 1s-.4 1-1 1h-1c-.6 0-1-.4-1-1s.4-1 1-1z"
                        ></path>
                      </svg>
                    </div>
                    <h3 className="heading mb-0">More Body Types</h3>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
