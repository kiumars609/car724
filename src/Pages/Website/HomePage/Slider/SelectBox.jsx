//  (ENG) This component about two select box. first, second select box is disabled and when you select first select box, then second select box is open.
// for example: select provance and then city.

// (DE) Diese component uber select box. zuerst, zweite select box ist schließe und wenn du wahle erste select box aus, dann zweite select box ist offnen.
// zum beispiel: wahlen bundesland und stadt aus.

import React from "react";
import { useState, useEffect } from "react";
import { data } from "./data";

export default function SelectBox({ getBr, getMd, setActiveSubmit }) {
  const { brands, cars } = data();
  const [active, setActive] = useState(true);
  const [autoBrands, setAutoBrands] = useState("");
  const [autoModels, setAutoModels] = useState("");

  // map first select (brands)
  const mapBrands =
    brands &&
    brands.map((item, index) => {
      return (
        <>
          <option key={index} value={item.value}>
            {item.title}
          </option>
        </>
      );
    });

  const selectBrand = (e) => {
    setAutoBrands(e.target.value);
  };

  const selectModel = (e) => {
    getMd(e.target.value);
  };

  useEffect(() => {
    if (autoBrands === "") {
      setActive(true);
      setAutoModels(
        <option value="all" selected>
          Select Your Model
        </option>
      );
    } else {
      setActive(false);
      setActiveSubmit(false);
      getBr(autoBrands);
      // map second select (select model of cars after select brand of car)
      const mapCars =
        cars &&
        cars.map((item, index) => {
          if (item.keyBrand === autoBrands) {
            return (
              <option key={index} value={item.value} className="models">
                {item.model}
              </option>
            );
          }
        });
      setAutoModels(mapCars);
    }
  }, [autoBrands]);

  return (
    <div className="mb-3 mx-auto">
      <select
        className="form-select"
        aria-label="Default select example"
        onChange={selectBrand}
      >
        <option value="" selected>
          Select Your Brand
        </option>
        {mapBrands}
      </select>

      <select
        className="form-select mt-3"
        aria-label="Disabled select example"
        disabled={active ? "disabled" : ""}
        onChange={selectModel}
      >
        <option value="all" selected>Select Your Model</option>
        {autoModels}
      </select>

      <select className="form-select mt-3" aria-label="Default select example">
        <option selected>Price until (€)</option>
        <option value="500">500 €</option>
        <option value="1000">1000 €</option>
        <option value="1500">1500 €</option>
        <option value="2000">2000 €</option>
        <option value="2500">2500 €</option>
        <option value="3000">3000 €</option>
        <option value="3500">3500 €</option>
        <option value="4000">4000 €</option>
        <option value="4500">4500 €</option>
        <option value="5000">5000 €</option>
        <option value="5500">5500 €</option>
        <option value="6000">6000 €</option>
        <option value="6500">6500 €</option>
        <option value="7000">7000 €</option>
      </select>

      <select className="form-select mt-3" aria-label="Default select example">
        <option selected>Registration date from</option>
        <option value="2023">2023</option>
        <option value="2022">2022</option>
        <option value="2021">2021</option>
        <option value="2020">2020</option>
        <option value="2019">2019</option>
        <option value="2018">2018</option>
        <option value="2017">2017</option>
        <option value="2016">2016</option>
        <option value="2015">2015</option>
        <option value="2014">2014</option>
        <option value="2013">2013</option>
        <option value="2012">2012</option>
        <option value="2011">2011</option>
        <option value="2010">2010</option>
      </select>
    </div>
  );
}
