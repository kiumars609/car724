import React from "react";
import FormBox from "./FormBox";

export default function Slider() {
  return (
    <>
      <div
        className="hero-wrap"
        style={{ backgroundImage: `url(/assets/images/background/newBmw.jpg)` }}
        data-stellar-background-ratio="0.5"
      >
        <div className="overlay"></div>
        <div className="container">
          <div className="row no-gutters slider-text justify-content-start align-items-center">
            <div className="col-lg-6 col-md-6 ftco-animate d-flex align-items-end">
              <div className="text">
                <h1 className="mb-4">
                  Now <span>It's easy for you</span>{" "}
                  <span>buy & sell a car</span>
                </h1>
                <p style={{ fontSize: "18px" }}>
                  A small river named Duden flows by their place and supplies it
                  with the necessary regelialia. It is a paradisematic country,
                  in which roasted parts
                </p>
                <a
                  href="https://vimeo.com/45830194"
                  className="icon-wrap popup-vimeo d-flex align-items-center mt-4"
                >
                  <div className="icon d-flex align-items-center justify-content-center">
                    <span className="ion-ios-play"></span>
                  </div>
                  <div className="heading-title ml-5">
                    <span>Easy steps for renting a car</span>
                  </div>
                </a>
              </div>
            </div>
            <div className="col-lg-2 col"></div>
            <FormBox />
          </div>
        </div>
      </div>
    </>
  );
}
