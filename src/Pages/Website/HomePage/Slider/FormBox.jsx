import React, { useState } from "react";
import SelectBox from "./SelectBox";
import { Navigate, useNavigate } from "react-router-dom";

export default function FormBox() {
  const [getBrand, setGetBrand] = useState("");
  const [getModel, setGetModel] = useState("all");
  const [activeSubmit, setActiveSubmit] = useState(true);

  const navigate = useNavigate();

  const getBr = (value) => {
    setGetBrand(value);
  };

  const getMd = (value) => {
    setGetModel(value);
  };

  const getSubmit = (e) => {
    e.preventDefault();
    navigate(`/results/${getBrand}/${getModel}`);
  };

  return (
    <>
      <div className="col-lg-4 col-md-6 mt-0 d-flex">
        <form onSubmit={getSubmit} className="request-form">
          <ul className="nav nav-tabs" id="myTab" role="tablist">
            <li className="nav-item" role="presentation">
              <button
                className="nav-link active"
                id="car-tab"
                data-bs-toggle="tab"
                data-bs-target="#car"
                type="button"
                role="tab"
                aria-controls="car"
                aria-selected="true"
              >
                <i className="fa fa-car" aria-hidden="true"></i>
              </button>
            </li>
            <li className="nav-item" role="presentation">
              <button
                className="nav-link disabled"
                id="motorcycle-tab"
                data-bs-toggle="tab"
                data-bs-target="#motorcycle"
                type="button"
                role="tab"
                aria-controls="motorcycle"
                aria-selected="false"
              >
                <i className="fa fa-motorcycle" aria-hidden="true"></i>
              </button>
            </li>
            <li className="nav-item" role="presentation">
              <button
                className="nav-link disabled"
                id="truck-tab"
                data-bs-toggle="tab"
                data-bs-target="#truck"
                type="button"
                role="tab"
                aria-controls="truck"
                aria-selected="false"
              >
                <i className="fa fa-truck" aria-hidden="true"></i>
              </button>
            </li>
          </ul>
          <div className="tab-content form-box" id="myTabContent">
            {/* Car Tab */}
            <div
              className="tab-pane fade show active form-box-cars"
              id="car"
              role="tabpanel"
              aria-labelledby="car-tab"
            >
              <div className="col-md-12 p-0 form-group mt-3 mx-5">
                <SelectBox
                  getBr={getBr}
                  getMd={getMd}
                  setActiveSubmit={setActiveSubmit}
                />
              </div>
              <div className="form-group">
                <input
                  type="submit"
                  value="Search Vehicle"
                  className="btn btn-primary py-3 px-4"
                  disabled={activeSubmit ? "disabled" : ""}
                />
              </div>
            </div>

            {/* Motorcycle Tab */}
            <div
              className="tab-pane fade"
              id="motorcycle"
              role="tabpanel"
              aria-labelledby="motorcycle-tab"
            >
              ...
            </div>
            {/* Truck Tab */}
            <div
              className="tab-pane fade"
              id="truck"
              role="tabpanel"
              aria-labelledby="truck-tab"
            >
              ...
            </div>
          </div>
        </form>
      </div>
    </>
  );
}
