import React, { Component } from "react";
import HomePage from "../src/Pages/Website/HomePage";
import CarDetail from "./Pages/Website/CarDetail/";
import { Route, Routes } from "react-router-dom";
import { useScrollToTop } from "./CustomHooks/useScrollToTop";
import BodyTypeDetails from "./Pages/Website/BodyTypeDetails";
import SearchingCars from "./Pages/Website/SearchingCars/";
import Error404 from "./Pages/Error404/Error404";
import Brands from "./Pages/Website/Brands/Brands";


function App() {
  useScrollToTop();
  return (
    <>
      <Routes>
        <Route path="/" exact element={<HomePage />} />
        <Route path="/car-detail/:id" element={<CarDetail />} />
        <Route path="/body-type/:type" element={<BodyTypeDetails />} />
        <Route path="/results/:brand/:model" element={<SearchingCars />} />
        <Route path="/brands/:brand/" element={<Brands />} />
        {/* 404 Rout */}
        <Route path="/*" element={<Error404 />} />
      </Routes>
    </>
  );
}

export default App;
